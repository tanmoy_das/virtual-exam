$(function () {
    $.widget("custom.reorderQuestionsWidget", {
        options: {
            questionDtoList: [],
            modal: 'hide',
            target: {
                questionListId: null,
                updatableListId: null
            }
        },
        _create: function () {
            this.setupQuestions(this.options.questionDtoList);
            this.changeModalStatus(this.options.modal);
            this.makeListSortable(this.options.questionListId, this.options.updatableListId);
        },
        _setOption: function (key, value) {
            switch (key) {
                case "questionDtoList":
                    this.setupQuestions(value);
                    this.makeListSortable();
                    break;
                case 'modal':
                    this.changeModalStatus(value);
                    break;
                case "target":
                    this.makeListSortable();
                    break;
            }
        },
        changeModalStatus: function (status) {
            this.element.modal(status);
        },
        setupQuestions: function (questions) {
            let questionList = $(this.options.target.questionListId);

            questionList.text("");
            for (let questionDto of questions) {
                let listItem = $('<li>')
                    .addClass('list-group-item border border-secondary mb-3')
                    .attr('data-index', questionDto.questionId);

                let questionData = $('<div>').addClass('d-flex')
                    .append($('<h5>').text(`${questionDto.questionCode}: ${questionDto.questionTitle}`))
                    .append($('<p>').addClass('flex-fill ml-3').text(`[${questionDto.chapterName}]`));

                listItem.append(questionData);

                questionList.append(listItem);
            }
        },
        makeListSortable: function () {
            let questionList = $(this.options.target.questionListId);
            let updatableList = $(this.options.target.updatableListId);

            questionList.sortable({
                update: function (event, ui) {
                    let orderedQuestionIds = [];
                    $(this).children().each(function (index) {
                        let attr = $(this).attr('data-index');
                        orderedQuestionIds.push(parseInt(attr));
                    });
                    console.log(orderedQuestionIds);
                    let questionOrderJsonInput = $("input.questionOrderJson").eq(0);
                    questionOrderJsonInput.val(JSON.stringify(orderedQuestionIds));

                    updatableList.text("");
                    updatableList.append($(this).children().clone());
                }
            });
        }
    });
});
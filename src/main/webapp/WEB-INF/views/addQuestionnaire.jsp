<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <h2 class="mx-3">Add Questionnaire</h2>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                </div>

            </div>
        </nav>

        <div class="tab-content">
            <div class="alert-danger my-3">
                <form:errors path="questionnaireCommand.*"/>
            </div>

            <c:url var="addQuestionnaireLink" value="/questionnaire/submit"/>
            <form:form method="post" modelAttribute="questionnaireCommand" action="${addQuestionnaireLink}">

                <form:hidden path="completed" value="${false}"/>

                <div class="form-group">
                    <div>
                        <spring:message code="label.subjectName"/>:
                        <h5>
                            <c:out value="${questionnaireCommand.questionnaire.subject.label}"/>
                        </h5>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="chapters">
                        <spring:message code="label.chapterName"/>
                    </form:label>
                    <form:select path="chapters" cssClass="form-control" multiple="multiple">
                        <form:options items="${questionnaireCommand.questionnaire.subject.chapters}"
                                      itemValue="id"
                                      itemLabel="label"/>
                    </form:select>
                </div>

                <div class="form-group">
                    <form:label path="questionnaire.label">
                        <spring:message code="label.questionnaireLabel" var="questionnaireLabelText"/>
                        <c:out value="${questionnaireLabelText}"/>
                    </form:label>

                    <form:input path="questionnaire.label"
                                cssClass="form-control"
                                placeholder="${questionnaireLabelText}" min="1"/>
                </div>


                <div class="form-group">
                    <form:label path="numberOfQuestions">
                        <spring:message code="label.numberOfQuestions"/>
                    </form:label>

                    <form:input path="numberOfQuestions"
                                type="number"
                                cssClass="form-control"
                                aria-describedby="numberOfQuestionsHint"
                                placeholder="Number of Questions" min="1"/>

                    <small id="numberOfQuestionsHint" class="form-text text-muted">
                        <spring:message code="text.enterNumberOfQuestions"/>
                    </small>
                </div>


                <form:button name="_action_generate_questionnaire" class="btn btn-info">
                    <spring:message code="label.generateQuestionnaire"/>
                </form:button>

                <form:button name="_action_choose_questions" class="btn btn-info">
                    <spring:message code="label.chooseQuestionsManually"/>
                </form:button>
            </form:form>
        </div>
    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

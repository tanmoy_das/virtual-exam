<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <h2 class="mx-3">
                    <spring:message code="label.addQuestion"/>
                </h2>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                </div>

            </div>
        </nav>

        <div class="tab-content">
            <div class="my-5 alert-danger">
                <form:errors path="questionCommand.question.*"/>
            </div>

            <c:url var="addQuestionUrl" value="/question/submit"/>
            <form:form modelAttribute="questionCommand" action="${addQuestionUrl}" method="post">
                <c:set var="question" value="${questionCommand.question}"/>
                <c:set var="questionChapter" value="${question.chapter}"/>
                <c:set var="questionSubject" value="${questionChapter.subject}"/>

                <div class="form-group">
                    <div>
                        <spring:message code="label.subjectName"/>
                    </div>
                    <div>
                        <strong>
                            <c:out value="${questionSubject.label}"/>
                        </strong>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="question.chapter">
                        <spring:message code="label.chapterName"/>
                    </form:label>

                    <form:select path="question.chapter" class="form-control">
                        <form:options items="${questionSubject.chapters}" itemValue="id" itemLabel="label"/>
                    </form:select>
                </div>

                <div class="form-group">
                    <form:label path="question.code">
                        <spring:message code="label.questionCode" var="questionCodeLabel"/>
                        <c:out value="${questionCodeLabel}"/>
                    </form:label>
                    <form:input path="question.code" cssClass="form-control"
                                aria-describedby="questionCodeHint" placeholder="${questionCodeLabel}"/>
                    <small id="questionCodeHint" class="form-text text-muted">
                        <spring:message code="text.enterQuestionCode"/>
                    </small>
                </div>

                <div class="form-group">
                    <form:label path="question.title">
                        <spring:message code="label.questionTitle" var="questionTitle"/>
                        <c:out value="${questionTitle}"/>
                    </form:label>
                    <form:input path="question.title" cssClass="form-control"
                                aria-describedby="emailHelp" placeholder="${questionTitle}"/>
                    <small id="emailHelp" class="form-text text-muted">
                        <spring:message code="text.enterQuestionTitle"/>
                    </small>
                </div>

                <div class="form-group">
                    <form:label path="question.hint">
                        <spring:message code="label.questionHint" var="questionHint"/>
                        <c:out value="${questionHint}"/>
                    </form:label>
                    <form:input path="question.hint" cssClass="form-control"
                                aria-describedby="questionHintHelp" placeholder="${questionHint}"/>
                    <small id="questionHintHelp" class="form-text text-muted">
                        <spring:message code="text.enterQuestionHint"/>
                    </small>
                </div>

                <div class="form-group">
                    <form:label path="question.answerType">
                        <spring:message code="label.answerType" var="answerTypeLabel"/>
                        <c:out value="${answerTypeLabel}"/>
                    </form:label>
                    <form:select cssClass="form-control" path="question.answerType">
                        <%@ page import="net.therap.virtualexam.domain.AnswerType" %>
                        <c:forEach items="${AnswerType.values()}" var="answerType">
                            <form:option value="${answerType}">
                                <c:out value="${answerType.getData()}"/>
                            </form:option>
                        </c:forEach>
                    </form:select>
                </div>

                <div class="form-group">
                    <spring:message code="label.next" var="nextLabel"/>
                    <input type="submit" class="btn btn-primary" value="${nextLabel}">
                </div>
            </form:form>
        </div>
    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

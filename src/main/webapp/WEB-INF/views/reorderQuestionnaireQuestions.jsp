<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>

                <h2 class="mx-3">Reorder Questionnaire Questions</h2>

            </div>
        </nav>

        <div class="tab-content">
            <form:errors path="questionReorderCommand.*" cssClass="alert alert-danger my-3"/>

            <c:url var="questionReorderUrl" value="/questionnaire/update"/>
            <form:form method="post" modelAttribute="questionReorderCommand" action="${questionReorderUrl}">
                <c:set var="questionnaire" value="${questionReorderCommand.questionnaire}"/>

                <form:hidden path="questionnaire.questionOrderJson" cssClass="questionOrderJson"/>
                <div class="my-2">
                    <div class="card mb-2">
                        <div class="card-body">
                            <div class="d-flex flex-column">
                                <h5 class="card-title form-group align-self-start">
                                    <form:input path="questionnaire.label" cssClass="form-control"/>
                                </h5>
                                <h5 class="card-title flex-fill text-muted">
                                    <span>Subject: ${questionnaire.subject.label}</span>
                                </h5>
                            </div>
                            <span>
                            <b><spring:message code="label.chapters"/>:</b>
                            <c:forEach items="${questionnaire.chapterLabels}" var="chapterName">
                                <c:out value="${chapterName}"/>,
                            </c:forEach>
                        </span>
                        </div>


                        <p class="small text-sm-left text-muted ml-4">
                            <spring:message code="dragAndDropToReorder"/>
                        </p>

                    </div>

                    <div class="mb-1 d-flex">
                        <strong class="flex-fill">
                            Questions:
                        </strong>
                        <a href="#"
                           class="text-info"
                           onclick="showReorderModal('#qq${questionnaire.id}', '#qqq${questionnaire.id}')">
                            <u>Reorder Questions</u>
                        </a>
                    </div>

                    <ul class="list-group list-group-flush mb-3" id="qqq${questionnaire.id}">
                        <c:forEach items="${questionnaire.questions}" var="question">
                            <li class="list-group-item border border-secondary mb-3"
                                data-index="${question.id}">
                                <div class="d-flex">
                                    <h5>
                                        <c:out value="${question.code}"/>:
                                        <c:out value="${question.title}"/>
                                    </h5>
                                    <p class="flex-fill ml-3">
                                        [
                                        <c:out value="${question.chapter.label}"/>
                                        ]
                                    </p>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>

                    <div class="modal fade" id="reorderModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        <spring:message code="label.reorderQuestions"/>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <ul class="list-group list-group-flush card-body pr-0 border-secondary"
                                        id="qq${questionnaire.id}">
                                    </ul>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                                        Reorder Questions
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form:button name="_action_reorder_questions" class="btn btn-primary">
                        <spring:message code="label.saveChanges"/>
                    </form:button>

                </div>
            </form:form>
        </div>
    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js">
</script>

<script>
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>

<c:url var="reorderQuestionsWidgetUrl" value="/js/reorderQuestionsWidget.js"/>
<script src="${reorderQuestionsWidgetUrl}"></script>

<script type="text/javascript">
    function showReorderModal(questionListId, updatableListId) {
        let questionDtoList = JSON.parse('${questionReorderCommand.questionDtoList}');
        console.log(questionDtoList);

        $("#reorderModal").reorderQuestionsWidget({
            questionDtoList: questionDtoList,
            modal: 'show',
            target: {
                questionListId: questionListId,
                updatableListId: updatableListId
            }
        });
    }
</script>

</body>
</html>

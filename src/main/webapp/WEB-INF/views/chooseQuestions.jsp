<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <h2 class="mx-3">Choose ${questionnaireCommand.numberOfQuestions} Question(s)</h2>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                </div>

            </div>
        </nav>

        <div class="tab-content d-flex flex-column">
            <c:url var="saveQuestionnaireUrl" value="/questionnaire/submit"/>

            <form:errors path="questionnaireCommand.*" cssClass="alert alert-danger" element="div"/>

            <form:form modelAttribute="questionnaireCommand"
                       action="${saveQuestionnaireUrl}"
                       class="d-flex flex-column flex-fill">

                <form:hidden path="completed" value="${true}"/>

                <c:forEach items="${questionnaireCommand.availableQuestions}" var="question">
                    <c:if test="${question.status != Status.DELETED}">
                        <div class="form-group">
                            <div class="card flex-fill my-2">
                                <div class="card-header">
                                    <div class="form-check">
                                        <form:checkbox path="questionnaire.questions"
                                                       value="${question.id}"
                                                       cssClass="form-check-input"/>
                                        <form:label path="questionnaire.questions" class="form-check-label mx-2">
                                            <c:out value="${question.code}"/>
                                        </form:label>
                                    </div>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">${question.title}</li>
                                </ul>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>

                <form:button name="_action_save_questionnaire" class="btn btn-primary">
                    <spring:message code="label.save"/>
                </form:button>
            </form:form>
        </div>
    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

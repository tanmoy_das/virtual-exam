<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>

                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>
            </div>
        </nav>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="subjects">

                <center>
                    <h1>
                        <spring:message code="text.unexpectedError"/>
                    </h1>

                    <c:if test="${errorCode != null}">
                        <div class="alert alert-danger">
                            <spring:message code="${errorCode}"/>
                        </div>
                    </c:if>
                </center>

            </div>
        </div>
    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

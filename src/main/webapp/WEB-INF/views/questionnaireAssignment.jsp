<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <h2 class="mx-3">
                    <spring:message code="label.addSubject"/>
                </h2>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                </div>

            </div>
        </nav>

        <div class="tab-content">
            <form:errors path="questionnaireAssignmentCommand.*" element="div" cssClass="alert alert-danger"/>

            <c:url var="assignStudentUrl" value="/questionnaireAssignment/submit"/>
            <form:form modelAttribute="questionnaireAssignmentCommand" method="post" action="${assignStudentUrl}">
                <div class="form-group">
                    Questionnaire:
                    <h5>
                        <c:out value="${currentQuestionnaire.label}"/>
                    </h5>
                </div>

                <div class="form-group">
                    <form:label path="questionnaireAssignment.assignee">
                        <spring:message code="label.studentName" var="studentNameLabel"/>
                        <c:out value="${studentNameLabel}"/>
                    </form:label>
                    <form:select path="questionnaireAssignment.assignee"
                                 cssClass="form-control"
                                 placeholder="${studentNameLabel}">
                        <form:options items="${students}" itemLabel="fullName" itemValue="id"/>
                    </form:select>
                </div>
                <button type="submit" class="btn btn-primary">
                    Assign to Student
                </button>
            </form:form>

            <div class="my-5">
                Current Assignments: ${currentQuestionnaire.questionnaireAssignments.size()}

                <div class="d-flex">
                    <c:forEach items="${currentQuestionnaire.questionnaireAssignments}" var="assignment">
                        <c:if test="${assignment.status != Status.DELETED}">
                            <div class="toast m-2" role="alert" aria-live="assertive" aria-atomic="true">
                                <div class="toast-header">
                                    <strong class="mr-auto">
                                        <c:out value="${assignment.assignee.fullName}"/>
                                    </strong>
                                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        </div>


    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.toast').toast({
            'autohide': false
        });

        $('.toast').toast('show');

        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="net.therap.virtualexam.util.Constants" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="${homepageUrl}">
                <h3><spring:message code="label.app.name"/></h3>
            </a>
        </div>

        <ul class="list-unstyled components">
            <p>
                <spring:message code="label.app.description"/>
            </p>

            <div class="d-flex p-3">
                <a href="#" class="btn btn-light flex-fill">
                    Add Subject
                </a>
            </div>


            <li class="active">
                <div class="d-flex">
                    <a href="#" class="flex-fill">
                        Science
                    </a>
                    <a href="#homeSubmenu" data-toggle="collapse"
                       aria-expanded="false" class="dropdown-toggle">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a href="#" class="btn btn-outline-light">
                        <em class="fa fa-plus"></em>
                    </a>
                </div>
                <ul class="collapse show list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Chapter 1: Physics</a>
                    </li>
                    <li>
                        <a href="#">Chapter 2: Chemistry</a>
                    </li>
                    <li>
                        <a href="#">Chapter 3: Biology</a>
                    </li>
                </ul>
            </li>
            <li>
                <div class="d-flex">
                    <a href="#" class="flex-fill">
                        Math
                    </a>
                    <a href="#mathSubmenu" data-toggle="collapse"
                       aria-expanded="false" class="dropdown-toggle">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a href="#" class="btn btn-outline-light">
                        <em class="fa fa-plus"></em>
                    </a>
                </div>
                <ul class="collapse list-unstyled" id="mathSubmenu">
                    <li>
                        <a href="#">Chapter 1</a>
                    </li>
                    <li>
                        <a href="#">Chapter 2</a>
                    </li>
                    <li>
                        <a href="#">Chapter 3</a>
                    </li>
                </ul>
            </li>

        </ul>

        <ul class="list-unstyled CTAs">
            <li>
                <div class="d-flex justify-content-center">
                    <c:url var="bnUrl" value="?locale=bn"/>
                    <a href="${bnUrl}" class="nav-item mx-1">
                        <c:url var="bdFlagUrl" value="${Constants.BD_FLAG_IMG_LINK}"/>
                        <img src="${bdFlagUrl}" class="" alt="Bengali">
                    </a>

                    <c:url var="enUrl" value="?locale=en"/>
                    <a href="${enUrl}" class="nav-item mx-1">
                        <c:url var="usFlagUrl" value="${Constants.US_FLAG_IMG_LINK}"/>
                        <img src="${usFlagUrl}" class="" alt="English">
                    </a>
                </div>
            </li>

            <li>
                <a href="${logoutUrl}" class="btn btn-danger">
                    <spring:message code="label.logoutTxt"/>
                </a>
            </li>
        </ul>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <h2 class="mx-3">Science</h2>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Questions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Submissions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Questionnaires</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="mb-3 d-flex">
            <h2 class="flex-fill">Chapter 1: Physics</h2>
            <a href="#" class="btn btn-primary">Add Question</a>
        </div>

        <div class="my-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex mb-2">
                        <h5 class="card-title">Q01</h5><span class="mx-1">:</span>
                        <h5 class="card-title flex-fill">What is the name of this subject?</h5>
                        <div class="btn-group btn-group-sm align-self-start">
<%--                            <a href="#" class="btn btn-secondary">--%>
<%--                                <em class="fa fa-edit"></em>--%>
<%--                            </a>--%>
                            <a href="#" class="btn btn-danger">
                                <em class="fa fa-trash"></em>
                            </a>
                        </div>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>

                <div class="card-body btn-group-sm">
                    <a href="#answers1" data-toggle="collapse"
                       aria-expanded="false" class="card-link btn btn-outline-dark">
                        Show Options
                    </a>
                    <a href="#" class="card-link btn btn-outline-info">Add Option</a>
                </div>

                <ul class="collapse list-group list-group-flush" id="answers1">
                    <li class="list-group-item"></li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 1</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 2</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 3</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                </ul>

            </div>
        </div>

        <div class="my-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex mb-2">
                        <h5 class="card-title">Q02</h5><span class="mx-1">:</span>
                        <h5 class="card-title flex-fill">What is the name of this chapter?</h5>
                        <div class="btn-group btn-group-sm align-self-start">
<%--                            <a href="#" class="btn btn-secondary">--%>
<%--                                <em class="fa fa-edit"></em>--%>
<%--                            </a>--%>
                            <a href="#" class="btn btn-danger">
                                <em class="fa fa-trash"></em>
                            </a>
                        </div>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>

                <div class="card-body btn-group-sm">
                    <a href="#answers2" data-toggle="collapse"
                       aria-expanded="false" class="card-link btn btn-outline-dark">
                        Show Options
                    </a>
                    <a href="#" class="card-link btn btn-outline-info">Add Option</a>
                </div>

                <ul class="collapse show list-group list-group-flush" id="answers2">
                    <li class="list-group-item"></li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 1</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 2</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 3</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                </ul>

            </div>
        </div>

        <div class="line"></div>

        <div class="mb-3 d-flex">
            <h2 class="flex-fill">Chapter 2: Chemistry</h2>
            <a href="#" class="btn btn-primary">Add Question</a>
        </div>

        <div class="my-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex mb-2">
                        <h5 class="card-title">Q01</h5><span class="mx-1">:</span>
                        <h5 class="card-title flex-fill">What is the name of this subject?</h5>
                        <div class="btn-group btn-group-sm align-self-start">
<%--                            <a href="#" class="btn btn-secondary">--%>
<%--                                <em class="fa fa-edit"></em>--%>
<%--                            </a>--%>
                            <a href="#" class="btn btn-danger">
                                <em class="fa fa-trash"></em>
                            </a>
                        </div>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>

                <div class="card-body btn-group-sm">
                    <a href="#answers3" data-toggle="collapse"
                       aria-expanded="false" class="card-link btn btn-outline-dark">
                        Show Options
                    </a>
                    <a href="#" class="card-link btn btn-outline-info">Add Option</a>
                </div>

                <ul class="collapse list-group list-group-flush" id="answers3">
                    <li class="list-group-item"></li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 1</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 2</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 3</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                </ul>

            </div>
        </div>

        <div class="my-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex mb-2">
                        <h5 class="card-title">Q02</h5><span class="mx-1">:</span>
                        <h5 class="card-title flex-fill">What is the name of this chapter?</h5>
                        <div class="btn-group btn-group-sm align-self-start">
<%--                            <a href="#" class="btn btn-secondary">--%>
<%--                                <em class="fa fa-edit"></em>--%>
<%--                            </a>--%>
                            <a href="#" class="btn btn-danger">
                                <em class="fa fa-trash"></em>
                            </a>
                        </div>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>

                <div class="card-body btn-group-sm">
                    <a href="#answers4" data-toggle="collapse"
                       aria-expanded="false" class="card-link btn btn-outline-dark">
                        Show Options
                    </a>
                    <a href="#" class="card-link btn btn-outline-info">Add Option</a>
                </div>

                <ul class="collapse list-group list-group-flush" id="answers4">
                    <li class="list-group-item"></li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 1</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 2</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex">
                            <h5 class="flex-fill">Option 3</h5>
                            <div class="btn-group">
<%--                                <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                    <em class="fa fa-edit"></em>--%>
<%--                                </a>--%>
                                <a href="#" class="btn btn-sm btn-danger">
                                    <em class="fa fa-trash"></em>
                                </a>
                            </div>

                        </div>
                    </li>
                </ul>

            </div>
        </div>

        <div class="line"></div>

        <div class="mb-3 d-flex">
            <h2 class="flex-fill">Chapter 2: Chemistry</h2>
            <a href="#" class="btn btn-primary">Add Question</a>
        </div>

    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

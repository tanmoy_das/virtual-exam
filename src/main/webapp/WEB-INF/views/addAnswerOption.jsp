<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <h2 class="mx-3">
                    <spring:message code="label.addAnswerOption"/>
                </h2>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                </div>

            </div>
        </nav>

        <div class="tab-content">
            <c:url var="addAnswerOptionUrl" value="/answerOption/submit"/>
            <%--@elvariable id="answerOptionCommand" type="net.therap.virtualexam.command.AnswerOptionCommand"--%>
            <form:form modelAttribute="answerOptionCommand" method="post" action="${addAnswerOptionUrl}">
                <c:set var="answerOption" value="${answerOptionCommand.answerOption}"/>

                <div class="form-group">
                    <h5>
                        <span class="text-muted">
                            <spring:message code="label.questionCode"/>:
                        </span>
                        <span> <c:out value="${answerOption.question.code}"/></span>
                    </h5>
                    <h5>
                        <span class="text-muted">
                            <spring:message code="label.questionTitle"/>:
                        </span>
                        <span> <c:out value="${answerOption.question.title}"/></span>
                    </h5>
                    <h5>
                        <span class="text-muted">
                            <spring:message code="label.questionHint"/>:
                        </span>
                        <span> <c:out value="${answerOption.question.hint}"/></span>
                    </h5>

                </div>

                <div class="form-group">
                    <form:label path="answerOption.label">
                        <spring:message code="label.answerOptionLabel"/>
                    </form:label>
                    <form:input path="answerOption.label" cssClass="form-control"
                                aria-describedby="emailHelp" placeholder="Answer Option Text"/>
                    <small id="questionCodeHint" class="form-text text-muted">
                        <spring:message code="enterAnswerOptionText"/>
                    </small>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <form:checkbox path="answerOption.correct" cssClass="form-check-input"/>
                        <form:label path="answerOption.correct" cssClass="form-check-label">
                            <spring:message code="label.correctOption"/>
                        </form:label>
                    </div>
                </div>

                <%--                <c:url var="homepageUrl" value="/"/>--%>
                <%--                <a href="${homepageUrl}" type="submit" class="btn btn-primary">Save</a>--%>

                <form:button type="submit" class="btn btn-primary" name="_action_save">
                    Save
                </form:button>

                <form:button type="submit" class="btn btn-secondary" name="_action_save_and_add">
                    Save and Add Another Option
                </form:button>

            </form:form>
        </div>
    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

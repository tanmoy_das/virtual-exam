<%--
  @author tanmoy.das
  @since 5/2/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<spring:message code="label.app.description"/>">
    <meta name="author" content="<spring:message code="label.author.name"/>">

    <title><spring:message code="label.dashboard"/></title>

    <c:url var="dashboardCssUrl" value="/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="${dashboardCssUrl}">
</head>

<c:url var="homepageUrl" value="/"/>
<c:url var="logoutUrl" value="/logout"/>

<body>
<div class="wrapper">
    <!-- Sidebar  -->
    <%@ include file="/WEB-INF/includes/sections/leftSideBar.jsp" %>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span></span>
                </button>

                <h2 class="mx-3"><spring:message code="label.welcome"/>, ${currentUser.fullName}</h2>

                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>
            </div>
        </nav>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="subjects">
                <h2 class="mb-3"><spring:message code="label.subjects"/></h2>

                <div id="subjectList">
                    <c:forEach items="${subjects}" var="subject">
                        <c:if test="${subject.status != Status.DELETED}">
                            <div class="my-2">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex mb-2">
                                            <c:url var="subjectUrl" value="/subject/show?subjectId=${subject.id}"/>
                                            <a href="${subjectUrl}" class="card-title flex-fill">
                                                <h5 class="card-title flex-fill">
                                                    <c:out value="${subject.label}"/>
                                                </h5>
                                            </a>
                                            <div class="btn-group btn-group-sm align-self-start">
<%--                                                <a href="#" class="btn btn-secondary">--%>
<%--                                                    <em class="fa fa-edit"></em>--%>
<%--                                                </a>--%>

                                                <c:url var="deleteSubject" value="/subject/delete">
                                                    <c:param name="subjectId" value="${subject.id}"/>
                                                </c:url>
                                                <a href="${deleteSubject}" class="btn btn-danger">
                                                    <em class="fa fa-trash"></em>
                                                </a>
                                            </div>
                                        </div>
                                        <p class="card-text">
                                            Chapters: <c:out value="${subject.chapters.size()}"/>,
                                            Questionnaires: undefined
                                        </p>
                                    </div>

                                    <div class="card-body btn-group-sm">
                                        <a href="#chapters${subject.id}" data-toggle="collapse"
                                           aria-expanded="false" class="card-link btn btn-outline-dark">
                                            <spring:message code="label.showChapters"/>
                                        </a>

                                        <c:url var="addChapterUrl" value="/chapter/create?subjectId=${subject.id}"/>
                                        <a href="${addChapterUrl}" class="card-link btn btn-outline-info">
                                            <spring:message code="label.addChapter"/>
                                        </a>
                                    </div>

                                    <ul class="collapse list-group list-group-flush" id="chapters${subject.id}">
                                        <c:forEach items="${subject.chapters}" var="chapter">
                                            <c:if test="${chapter.status != Status.DELETED}">
                                                <li class="list-group-item">
                                                    <div class="d-flex">
                                                        <h5 class="flex-fill">
                                                            <c:out value="${chapter.label}"/>
                                                        </h5>
                                                        <div class="btn-group">
<%--                                                            <a href="#" class="btn btn-sm btn-secondary">--%>
<%--                                                                <em class="fa fa-edit"></em>--%>
<%--                                                            </a>--%>

                                                            <c:url var="chapterDeleteUrl" value="/chapter/delete">
                                                                <c:param name="chapterId" value="${chapter.id}"/>
                                                            </c:url>
                                                            <a href="${chapterDeleteUrl}" class="btn btn-sm btn-danger">
                                                                <em class="fa fa-trash"></em>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>
                                        </c:forEach>
                                    </ul>

                                </div>
                            </div>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
</body>
</html>

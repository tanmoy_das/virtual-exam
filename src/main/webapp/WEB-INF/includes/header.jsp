<%--
  @author tanmoy.das
  @since 4/29/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ page import="net.therap.virtualexam.util.Constants" %>

<c:url var="bootstrapCssUrl" value="/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${bootstrapCssUrl}">

<c:url var="fontAwesomeCssUrl" value="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"/>
<link rel="stylesheet" href="${fontAwesomeCssUrl}" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ="
      crossorigin="anonymous"/>

<%--<c:url var="simplebarCssUrl" value="https://unpkg.com/simplebar@latest/dist/simplebar.css"/>--%>
<%--<link rel="stylesheet" href="${simplebarCssUrl}">--%>

<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

<link href="https://code.jquery.com/ui/1.12.0/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

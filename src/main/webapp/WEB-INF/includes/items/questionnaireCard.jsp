<%--
  @author tanmoy.das
  @since 6/22/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="my-2">
    <div class="card">
        <div class="card-body">
            <div class="d-flex mb-2">
                <h5 class="card-title">${questionnaire.label}</h5>
                <h5 class="card-title flex-fill pl-3 text-muted">
                    <span>[Subject: ${questionnaire.subject.label}]</span>
                </h5>
                <div class="btn-group btn-group-sm align-self-start">
                    <c:url var="reorderQuestionsUrl" value="/questionnaire/edit">
                        <c:param name="questionnaireId" value="${questionnaire.id}"/>
                    </c:url>
                    <a href="${reorderQuestionsUrl}" class="btn btn-secondary">
                        <em class="fa fa-edit"></em>
                        Edit Questionnaire / Reorder Questions
                    </a>
                    <a href="#" class="btn btn-danger">
                        <em class="fa fa-trash"></em>
                    </a>
                </div>
            </div>
            <span>
                <b>
                    <spring:message code="label.chapters"/>:
                </b>
                <c:forEach items="${questionnaire.chapterLabels}" var="chapterName">
                    <c:out value="${chapterName}"/>,
                </c:forEach>
            </span>
        </div>

        <div class="card-body btn-group-sm">
            <a href="#qq${questionnaire.id}" data-toggle="collapse"
               aria-expanded="false" class="card-link btn btn-outline-dark">
                <spring:message code="label.showQuestions"/>
            </a>
            <c:url var="questionnaireAssignmentPageUrl"
                   value="/questionnaireAssignment/index?questionnaireId=${questionnaire.id}"/>
            <a href="${questionnaireAssignmentPageUrl}" class="card-link btn btn-outline-success">
                <spring:message code="label.assignQuestionnaire"/>
            </a>
            <span class="card-link">
                            <spring:message code="label.numberOfQuestions"/>:
                            <b><c:out value="${questionnaire.questions.size()}"/></b>
                        </span>
        </div>

        <ul class="collapse list-group list-group-flush" id="qq${questionnaire.id}">
            <li class="list-group-item"></li>
            <c:forEach items="${questionnaire.questions}" var="question">
                <li class="list-group-item">
                    <div class="d-flex">
                        <h5>
                            <c:out value="${question.code}"/>:
                            <c:out value="${question.title}"/>
                        </h5>
                        <p class="flex-fill ml-3">
                            [
                            <spring:message code="label.chapterName"/>:
                            <c:out value="${question.chapter.label}"/>
                            ]
                        </p>
                        <div class="btn-group">
<%--                            <a href="#" class="btn btn-sm btn-secondary align-self-start">--%>
<%--                                <em class="fa fa-edit"></em>--%>
<%--                            </a>--%>
                            <a href="#" class="btn btn-sm btn-danger align-self-start">
                                <em class="fa fa-trash"></em>
                            </a>
                        </div>

                    </div>
                </li>
            </c:forEach>
        </ul>

    </div>
</div>
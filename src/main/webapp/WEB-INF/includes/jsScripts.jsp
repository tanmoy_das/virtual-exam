<%--
  @author tanmoy.das
  @since 4/29/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:url var="jqueryUrl" value="/js/jquery-3.4.1.min.js"/>
<script src="${jqueryUrl}"></script>

<c:url var="bootstrapJsUrl" value="/js/bootstrap.min.js"/>
<script src="${bootstrapJsUrl}"></script>

<c:url var="fontAwesomeJsUrl" value="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"/>
<script src="${fontAwesomeJsUrl}" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs="
        crossorigin="anonymous"></script>

<c:url var="holderJsUrl" value="/js/holder.min.js"/>
<script src="${holderJsUrl}"></script>

<c:url var="popperJsUrl" value="/js/popper.min.js"/>
<script src="${popperJsUrl}"></script>

<c:url var="tinymceJsUrl"
       value="https://cdn.tiny.cloud/1/p1p9mmswl4ky9g4tc92i72m8ql0udlurva2jymfgfbfwztzv/tinymce/5/tinymce.min.js"/>
<script src="${tinymceJsUrl}" referrerpolicy="origin"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

<!-- Graphs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

<%--<c:url var="simplebarJsUrl" value="https://unpkg.com/simplebar@latest/dist/simplebar.min.js"/>--%>
<%--<script src="${simplebarJsUrl}"></script>--%>

<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
        integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
        crossorigin="anonymous"></script>
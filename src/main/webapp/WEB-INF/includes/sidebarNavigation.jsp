<%--
  @author tanmoy.das
  @since 5/3/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="net.therap.virtualexam.util.Constants" %>

<nav class="col-md-2 col-xl-1 d-none d-md-block bg-light sidebar p-0">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item justify-content-center text-center">
                <c:url var="dashboardUrl" value="${Constants.DASHBOARD_PATH}"/>
                <a class="nav-link <c:if test="${isDashboardPage}">active</c:if>" href="${dashboardUrl}">
                    <em class="fa fa-home fa-3x circle-icon"></em>
                    <spring:message code="label.dashboard"/>
                    <c:if test="${isDashboardPage}">
                        <span class="sr-only">(current)</span>
                    </c:if>
                </a>
            </li>

            <li class="nav-item justify-content-center text-center">
                <c:url var="messagesUrl" value="${Constants.MESSAGES_PAGE_PATH}"/>
                <a class="nav-link <c:if test="${isMessagesPage}">active</c:if>" href="${messagesUrl}">
                    <em class="fa fa-envelope fa-3x circle-icon"></em>
                    <spring:message code="label.messages"/>
                    <c:if test="${isMessagesPage}">
                        <span class="sr-only">(current)</span>
                    </c:if>
                </a>
            </li>

            <li class="nav-item justify-content-center text-center">
                <c:url var="notesUrl" value="${Constants.NOTES_PAGE_PATH}"/>
                <a class="nav-link <c:if test="${isNotesPage}">active</c:if>" href="${notesUrl}">
                    <em class="fa fa-book fa-3x circle-icon"></em>
                    <spring:message code="label.notes"/>
                    <c:if test="${isNotesPage}">
                        <span class="sr-only">(current)</span>
                    </c:if>
                </a>
            </li>

            <li class="nav-item justify-content-center text-center">
                <c:url var="tasksUrl" value="${Constants.TASKS_PAGE_PATH}"/>
                <a class="nav-link <c:if test="${isTasksPage}">active</c:if>" href="${tasksUrl}">
                    <em class="fa fa-tasks fa-3x circle-icon"></em>
                    <spring:message code="label.tasks"/>
                    <c:if test="${isTasksPage}">
                        <span class="sr-only">(current)</span>
                    </c:if>
                </a>
            </li>

            <li class="d-none nav-item justify-content-center text-center">
                <c:url var="reportsUrl" value="${Constants.REPORTS_PAGE_PATH}"/>
                <a class="nav-link <c:if test="${isReportsPage}">active</c:if>" href="${reportsUrl}">
                    <em class="fa fa-flag fa-3x circle-icon"></em>
                    <spring:message code="label.reports"/>
                    <c:if test="${isReportsPage}">
                        <span class="sr-only">(current)</span>
                    </c:if>
                </a>
            </li>

            <li class="nav-item justify-content-center text-center">
                <c:url var="profileUrl" value="${Constants.PROFILE_PAGE_PATH}"/>
                <a class="nav-link <c:if test="${isProfilePage}">active</c:if>" href="${profileUrl}">
                    <em class="fa fa-user fa-3x circle-icon"></em>
                    <spring:message code="label.profile"/>
                    <c:if test="${isProfilePage}">
                        <span class="sr-only">(current)</span>
                    </c:if>
                </a>
            </li>
        </ul>
    </div>
</nav>
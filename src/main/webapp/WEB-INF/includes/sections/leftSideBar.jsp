<%--
  @author tanmoy.das
  @since 6/7/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="net.therap.virtualexam.util.Constants" %>
<%@ page import="net.therap.virtualexam.domain.Status" %>

<nav id="sidebar">
    <div class="sidebar-header">
        <a href="${homepageUrl}">
            <h3><spring:message code="label.app.name"/></h3>
        </a>
        <p class="text-light">
            <spring:message code="label.app.description"/>
        </p>
    </div>

    <ul class="list-unstyled components">


        <div class="line" style="margin: 0px; padding: 0px"></div>

        <div class="d-flex px-3 py-1 pt-3">
            <c:url var="addSubjectUrl" value="/subject/create"/>
            <a href="${addSubjectUrl}" class="btn btn-light flex-fill">
                <spring:message code="label.addSubject"/>
            </a>
        </div>

        <div class="d-flex px-3 py-1">
            <c:choose>
                <c:when test="${currentSubject == null}">
                    <c:url var="addChapterUrl" value="/chapter/create?subjectId=0"/>
                </c:when>
                <c:otherwise>
                    <c:url var="addChapterUrl" value="/chapter/create?subjectId=${currentSubject.id}"/>
                </c:otherwise>
            </c:choose>

            <a href="${addChapterUrl}" class="btn btn-outline-light flex-fill">
                <em class="fa fa-plus"></em>
                <spring:message code="label.addChapter"/>
            </a>
        </div>

        <li class="">
            <ul class="list-unstyled components">
                <c:forEach items="${subjects}" var="subject">
                    <c:if test="${subject.status != Status.DELETED}">
                        <li class="">
                            <div class="d-flex">
                                <c:url var="subjectUrl" value="/subject/show?subjectId=${subject.id}"/>
                                <a href="${subjectUrl}" class="flex-fill">
                                        ${subject.label}
                                </a>
                                <a href="#homeSubmenu${subject.id}" data-toggle="collapse"
                                   aria-expanded="false" class="dropdown-toggle">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </a>
                            </div>
                            <ul class="collapse list-unstyled" id="homeSubmenu${subject.id}">
                                <c:forEach items="${subject.chapters}" var="chapter">
                                    <c:if test="${chapter.status != Status.DELETED}">
                                        <li>
                                            <a href="#">
                                                <c:out value="${chapter.label}"/>
                                            </a>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </li>
                    </c:if>
                </c:forEach>
            </ul>
        </li>
    </ul>

    <ul class="list-unstyled CTAs">
        <li>
            <div class="d-flex justify-content-center">
                <c:url var="bnUrl" value="?locale=bn"/>
                <a href="${bnUrl}" class="nav-item mx-1">
                    <c:url var="bdFlagUrl" value="${Constants.BD_FLAG_IMG_LINK}"/>
                    <img src="${bdFlagUrl}" class="" alt="Bengali">
                </a>

                <c:url var="enUrl" value="?locale=en"/>
                <a href="${enUrl}" class="nav-item mx-1">
                    <c:url var="usFlagUrl" value="${Constants.US_FLAG_IMG_LINK}"/>
                    <img src="${usFlagUrl}" class="" alt="English">
                </a>
            </div>
        </li>

        <li>
            <a href="${logoutUrl}" class="btn btn-danger">
                <spring:message code="label.logoutTxt"/>
            </a>
        </li>
    </ul>
</nav>
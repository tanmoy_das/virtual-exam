<%--
  @author tanmoy.das
  @since 6/11/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="net.therap.virtualexam.domain.Status" %>

<div role="tabpanel" class="tab-pane active" id="questions">
    <%--@elvariable id="currentSubject" type="net.therap.virtualexam.domain.Subject"--%>
    <c:forEach items="${currentSubject.chapters}" var="chapter">
        <c:if test="${chapter.status != Status.DELETED}">
            <div class="mb-3 d-flex">
                <h2 class="flex-fill">
                    <a href="#questions${chapter.id}" data-toggle="collapse"
                       aria-expanded="false" class="card-link btn btn-outline-dark">
                        <c:out value="${chapter.label}"/>
                    </a>

                    <c:url var="chapterDeleteUrl" value="/chapter/delete">
                        <c:param name="chapterId" value="${chapter.id}"/>
                    </c:url>
                    <div class="btn-group">
                        <a href="${chapterDeleteUrl}" class="btn btn-danger">
                            <em class="fa fa-trash"></em>
                        </a>
                    </div>
                </h2>
                <c:url var="addQuestionUrl" value="/question/create?chapterId=${chapter.id}"/>
                <a href="${addQuestionUrl}" class="btn btn-primary">
                    <spring:message code="label.addQuestion"/>
                </a>
            </div>

            <div id="questions${chapter.id}" class="collapse show">
                <c:forEach items="${chapter.questions}" var="question">
                    <c:if test="${question.status != Status.DELETED}">
                        <div class="my-2">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex mb-2">
                                        <h5 class="card-title">
                                            <c:out value="${question.code}"/></h5><span class="mx-1">:</span>
                                        <h5 class="card-title flex-fill">
                                            <c:out value="${question.title}"/>
                                        </h5>
                                        <div class="btn-group btn-group-sm align-self-start">
<%--                                            <a href="#" class="btn btn-secondary">--%>
<%--                                                <em class="fa fa-edit"></em>--%>
<%--                                            </a>--%>

                                            <c:url var="deleteQuestionUrl" value="/question/delete">
                                                <c:param name="questionId" value="${question.id}"/>
                                            </c:url>
                                            <a href="${deleteQuestionUrl}" class="btn btn-danger">
                                                <em class="fa fa-trash"></em>
                                            </a>
                                        </div>
                                    </div>
                                    <p class="card-text">
                                        <c:out value="${question.hint}"/>
                                    </p>
                                </div>

                                <div class="card-body btn-group-sm">
                                    <p>
                                        <spring:message code="label.questionType"/>:
                                        <c:out value="${question.answerType}"/>
                                    </p>
                                    <c:if test="${!question.isWrittenQuestion()}">
                                        <a href="#answers${question.id}" data-toggle="collapse"
                                           aria-expanded="false" class="card-link btn btn-outline-dark">
                                            <spring:message code="label.showOptions"/>
                                        </a>
                                        <c:url var="addAnswerOptionUrl"
                                               value="/answerOption/create?questionId=${question.id}"/>
                                        <a href="${addAnswerOptionUrl}" class="card-link btn btn-outline-info">
                                            <spring:message code="label.addOption"/>
                                        </a>
                                    </c:if>
                                </div>

                                <ul class="collapse list-group list-group-flush" id="answers${question.id}">
                                    <li class="list-group-item"></li>
                                    <c:forEach items="${question.options}" var="answerOption">
                                        <li class="list-group-item">
                                            <div class="d-flex">
                                                <h5 class="flex-fill">
                                                    <c:out value="${answerOption.label}"/>
                                                    <c:if test="${answerOption.correct}">
                                                        <em class="fa fa-check ml-3 text-success"></em>
                                                    </c:if>
                                                </h5>
                                                <div class="btn-group">
<%--                                                    <a href="#" class="btn btn-sm btn-secondary align-self-start">--%>
<%--                                                        <em class="fa fa-edit"></em>--%>
<%--                                                    </a>--%>
                                                    <a href="#" class="btn btn-sm btn-danger align-self-start">
                                                        <em class="fa fa-trash"></em>
                                                    </a>
                                                </div>

                                            </div>
                                        </li>
                                    </c:forEach>
                                </ul>

                            </div>
                        </div>
                    </c:if>
                </c:forEach>


            </div>

            <div class="line"></div>
        </c:if>
    </c:forEach>

</div>
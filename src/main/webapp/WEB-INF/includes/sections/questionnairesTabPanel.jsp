<%--
  @author tanmoy.das
  @since 6/11/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div role="tabpanel" class="tab-pane" id="questionnaires">

    <div id="questionnaire1" class="">
        <div class="mb-3 d-flex">
            <c:url var="addQuestionnaireUrl" value="/questionnaire/create">
                <c:param name="subjectId" value="${currentSubject.id}"/>
            </c:url>
            <a href="${addQuestionnaireUrl}" class="btn btn-primary">Add Questionnaire</a>
        </div>

        <c:forEach items="${currentSubject.questionnaires}" var="questionnaire">
            <c:if test="${questionnaire.status != Status.DELETED}">
                <%@ include file="/WEB-INF/includes/items/questionnaireCard.jsp" %>
            </c:if>
        </c:forEach>

    </div>
</div>


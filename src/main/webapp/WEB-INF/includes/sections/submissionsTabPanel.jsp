<%--
  @author tanmoy.das
  @since 6/11/20
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div role="tabpanel" class="tab-pane" id="submissions">
    <div class="mb-3 d-flex">
        <h2 class="flex-fill">
            <a href="#submissions1" data-toggle="collapse"
               aria-expanded="false" class="card-link btn btn-outline-dark">
                Sheikh Junayed Ahmed Rahnum
            </a>
        </h2>
    </div>

    <div id="submissions1" class="collapse show">
        <div class="my-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex mb-2">
                        <h5 class="card-title">QQ207</h5><span class="mx-1">:</span>
                        <h5 class="card-title">CSE 1/1 Physics Questionnaire</h5>
                        <h5 class="card-title flex-fill pl-3 text-muted">
                            [Subject: Science, Chapters: Chapter 1: Physics, Chapter 2: Chemistry]</h5>
                        <div class="btn-group btn-group-sm align-self-start">
<%--                            <a href="#" class="btn btn-secondary">--%>
<%--                                <em class="fa fa-edit"></em>--%>
<%--                            </a>--%>
                            <a href="#" class="btn btn-danger">
                                <em class="fa fa-trash"></em>
                            </a>
                        </div>
                    </div>
                    <p class="card-text">Some quick example text to build on the card title and make up the
                        bulk of the card's content.</p>
                </div>

                <div class="card-body btn-group-sm">
                    <a href="#answers12" data-toggle="collapse"
                       aria-expanded="false" class="card-link btn btn-outline-dark">
                        Show Written Answers
                    </a>
                    <a href="#" class="card-link btn btn-outline-info">Add Option</a>
                    <span class="card-link">MCQ Score: <b>18/20</b></span>
                </div>

                <ul class="collapse list-group list-group-flush" id="answers12">
                    <li class="list-group-item"></li>
                    <li class="list-group-item">
                        <div class="d-flex"><b class="text-muted">Question: </b> What is the capital of BD?
                        </div>
                        <div class="d-flex">
                            <h5><em class="text-muted">Answer: </em> Dhaka</h5>
                            <h5 class="flex-fill"></h5>
                            <div class="form-group">
                                <input type="number" class="form-control"
                                       placeholder="Score" min="0" max="5">
                            </div>

                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex"><b class="text-muted">
                            Question: </b> What is the capital of USA?
                        </div>
                        <div class="d-flex">
                            <h5 class="flex-fill"><em class="text-muted">Answer: </em> Dhaka</h5>

                            <div class="form-group">
                                <input type="number" class="form-control"
                                       placeholder="Score" min="0" max="5">
                            </div>

                        </div>
                    </li>
                    <button class="btn btn-warning">Submit Score</button>
                </ul>

            </div>
        </div>
    </div>
</div>
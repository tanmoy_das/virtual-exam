package net.therap.virtualexam.util;

/**
 * @author tanmoy.das
 * @since 5/2/20
 */
public class Constants {

    public static final String HOMEPAGE_PATH = "/";
    public static final String DASHBOARD_PATH = "/dashboard";
    public static final String STUDENT_HOMEPAGE_PATH = "/home";
    public static final String PROFILE_PATH = "/profile";
    public static final String SUBJECT_PATH_PREFIX = "/subject";
    public static final String ERROR_PATH = "/error";

    public static final String PERSISTENCE_UNIT_NAME = "virtualexam";

    public static final String CURRENT_USER_LABEL = "currentUser";
    public static final String SUBJECTS_LABEL = "subjects";
    public static final String STATUS_LABEL = "status";
    public static final String CODE_LABEL = "code";


    public static final String IS_REQUEST_RECEIVED = "isRequestReceived";
    public static final String IS_MYSELF = "isMyself";
    public static final String CONNECTION_REQUESTS_LABEL = "connectionRequests";

    public static final String ACCEPT_CONNECTION_PATH = "/connection/accept";
    public static final String BD_FLAG_IMG_LINK = "/img/bd-flag.png";
    public static final String CANCEL_CONNECTION_PATH = "/connection/cancel";
    public static final String CREATE_NOTE_COMMENT_PATH = "/noteComment";
    public static final String CREATE_NOTE_PATH = "/note/new";
    public static final String CREATE_TASK_COMMENT_PATH = "/taskComment";
    public static final String CREATE_TASK_PATH = "/task/new";
    public static final String DELETE_MESSAGE_PATH = "/message/delete";
    public static final String DELETE_NOTE_ACCESS_PATH = "/noteAccess/delete";
    public static final String DELETE_NOTE_COMMENT_PATH = "/noteComment/delete";
    public static final String DELETE_NOTE_PATH = "/note/delete";
    public static final String DELETE_TASK_ASSIGNMENT_PATH = "/taskAssignment/delete";
    public static final String DELETE_TASK_COMMENT_PATH = "/taskComment/delete";
    public static final String DELETE_TASK_PATH = "/task/delete";
    public static final String LOGIN_PATH = "/login";
    public static final String LOGOUT_PATH = "/logout";
    public static final String MESSAGES_BASE_PATH = "/messages";
    public static final String MESSAGES_PAGE_PATH = "/messages";
    public static final String NOTE_ACCESS_PATH = "/noteAccess";
    public static final String NOTE_BASE_PATH = "/note";
    public static final String NOTES_PAGE_PATH = "/notes";
    public static final String NOTES_TASKS_FAVICON_LINK = "/img/spidey.png";
    public static final String NOTES_TASKS_LOGO_LINK = "/img/spidey.png";
    public static final String PROFILE_BASE_PATH = "/profile";
    public static final String PROFILE_PAGE_PATH = "/profile";
    public static final String REGISTER_PATH = "/register";
    public static final String REJECT_CONNECTION_PATH = "/connection/reject";
    public static final String REMOVE_CONNECTION_PATH = "/connection/remove";
    public static final String REPORTS_PAGE_PATH = "/reports";
    public static final String SEARCHACCEPT_CONNECTION_PATH_PAGE_PATH = "/search";
    public static final String SEND_CONNECTION_REQUEST_PATH = "/connection/send";
    public static final String SEND_MESSAGE_PATH = "/messages";
    public static final String TASK_ASSIGNMENT_PATH = "/taskAssignment";
    public static final String TASK_BASE_PATH = "/task";
    public static final String TASKS_PAGE_PATH = "/tasks";
    public static final String UPDATE_NOTE_PATH = "/note/update";
    public static final String UPDATE_PROFILE_PATH = "/profile/update";
    public static final String UPDATE_TASK_PATH = "/task/update";
    public static final String US_FLAG_IMG_LINK = "/img/us-flag.png";
    public static final String NOTE_COMMENT_COMMAND_NAME = "noteCommentCommand";
}

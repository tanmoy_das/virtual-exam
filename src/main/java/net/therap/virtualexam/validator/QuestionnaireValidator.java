package net.therap.virtualexam.validator;


import net.therap.virtualexam.command.QuestionReorderCommand;
import net.therap.virtualexam.command.QuestionnaireCommand;
import net.therap.virtualexam.domain.Chapter;
import net.therap.virtualexam.service.ChapterService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author tanmoy.das
 * @since 5/8/20
 */
@Component
public class QuestionnaireValidator implements Validator {

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private Logger logger;

    @Override
    public boolean supports(Class<?> clazz) {
        return QuestionnaireCommand.class.equals(clazz) ||
                QuestionReorderCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (target instanceof QuestionReorderCommand) {

        } else {
            QuestionnaireCommand questionnaireCommand = (QuestionnaireCommand) target;

            int totalQuestions = 0;
            for (Chapter chapter : questionnaireCommand.getChapters()) {
                totalQuestions += chapter.getQuestions().size();
            }

            if (totalQuestions < questionnaireCommand.getNumberOfQuestions()) {
                logger.warn("Chapters don't have enough questionDtoList to create questionnaire");

                errors.rejectValue("numberOfQuestions", "error.insufficientQuestionsInChapters");
            }

            if (questionnaireCommand.isCompleted()) {
                int existingQuestionsCount = questionnaireCommand.getQuestionnaire().getQuestions().size();
                if (existingQuestionsCount < questionnaireCommand.getNumberOfQuestions()) {
                    logger.warn("Insufficient number of questionDtoList");

                    errors.rejectValue("numberOfQuestions", "error.insufficientQuestionsInQuestionnaire");
                }

                if (existingQuestionsCount > questionnaireCommand.getNumberOfQuestions()) {
                    logger.warn("Exceeding limits on number of questionDtoList");

                    errors.rejectValue("numberOfQuestions", "error.extraQuestionsInQuestionnaire");

                }
            }
        }

    }
}

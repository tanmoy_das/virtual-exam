package net.therap.virtualexam.validator;


import net.therap.virtualexam.command.ChapterCommand;
import net.therap.virtualexam.domain.Chapter;
import net.therap.virtualexam.domain.Status;
import net.therap.virtualexam.domain.Subject;
import net.therap.virtualexam.service.ChapterService;
import net.therap.virtualexam.service.SubjectService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 5/8/20
 */
@Component
public class ChapterValidator implements Validator {

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private Logger logger;

    @Override
    public boolean supports(Class<?> clazz) {
        return ChapterCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ChapterCommand chapterCommand = (ChapterCommand) target;
        Chapter chapter = chapterCommand.getChapter();

        Optional<Subject> subjectOptional = subjectService.find(chapter.getSubject());

        if (!subjectOptional.isPresent()) {
            errors.rejectValue("chapter.subject", "error.invalidSubject");
        } else {
            Subject subject = subjectOptional.get();

            boolean duplicateChapterFound = subject.getChapters()
                    .stream()
                    .filter(persistedChapter -> persistedChapter.getStatus() != Status.DELETED)
                    .anyMatch(persistedChapter -> hasSameLabel(chapter, persistedChapter));

            if (duplicateChapterFound) {
                errors.rejectValue("chapter.label", "error.duplicateChapterName");
            }
        }
    }

    private boolean hasSameLabel(Chapter chapter, Chapter persistedChapter) {
        return persistedChapter.getLabel().equals(chapter.getLabel());
    }
}

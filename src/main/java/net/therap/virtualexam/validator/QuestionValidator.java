package net.therap.virtualexam.validator;


import net.therap.virtualexam.command.QuestionCommand;
import net.therap.virtualexam.domain.Question;
import net.therap.virtualexam.service.QuestionService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author tanmoy.das
 * @since 5/8/20
 */
@Component
public class QuestionValidator implements Validator {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private Logger logger;

    @Override
    public boolean supports(Class<?> clazz) {
        return QuestionCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        QuestionCommand questionCommand = (QuestionCommand) target;
        Question question = questionCommand.getQuestion();

        if (questionService.findByCode(question.getCode()).isPresent()) {
            logger.warn("Question with duplicate code found [code={}]", question.getCode());

            errors.rejectValue("question.code", "error.duplicateCodeError");
        }
    }
}

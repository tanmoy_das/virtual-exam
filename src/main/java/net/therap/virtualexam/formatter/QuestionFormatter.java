package net.therap.virtualexam.formatter;

import net.therap.virtualexam.domain.Question;
import net.therap.virtualexam.exception.InvalidChapterIdException;
import net.therap.virtualexam.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

/**
 * @author tanmoy.das
 * @since 6/8/20
 */
@Component
public class QuestionFormatter implements Formatter<Question> {

    @Autowired
    private QuestionService questionService;

    @Override
    public Question parse(String idTxt, Locale locale) throws ParseException {
        long id = Long.parseLong(idTxt);

        return questionService.find(id).orElseThrow(InvalidChapterIdException::new);
    }

    @Override
    public String print(Question question, Locale locale) {
        return String.valueOf(question.getId());
    }
}

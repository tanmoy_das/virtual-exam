package net.therap.virtualexam.formatter;

import net.therap.virtualexam.domain.User;
import net.therap.virtualexam.exception.InvalidChapterIdException;
import net.therap.virtualexam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

/**
 * @author tanmoy.das
 * @since 6/8/20
 */
@Component
public class UserFormatter implements Formatter<User> {

    @Autowired
    private UserService userService;

    @Override
    public User parse(String idTxt, Locale locale) throws ParseException {
        long id = Long.parseLong(idTxt);

        return userService.find(id).orElseThrow(InvalidChapterIdException::new);
    }

    @Override
    public String print(User user, Locale locale) {
        return String.valueOf(user.getId());
    }
}

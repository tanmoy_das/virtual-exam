package net.therap.virtualexam.formatter;

import net.therap.virtualexam.domain.Subject;
import net.therap.virtualexam.exception.InvalidChapterIdException;
import net.therap.virtualexam.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

/**
 * @author tanmoy.das
 * @since 6/8/20
 */
@Component
public class SubjectFormatter implements Formatter<Subject> {

    @Autowired
    private SubjectService subjectService;

    @Override
    public Subject parse(String idTxt, Locale locale) throws ParseException {
        long id = Long.parseLong(idTxt);

        return subjectService.find(id).orElseThrow(InvalidChapterIdException::new);
    }

    @Override
    public String print(Subject subject, Locale locale) {
        return String.valueOf(subject.getId());
    }
}

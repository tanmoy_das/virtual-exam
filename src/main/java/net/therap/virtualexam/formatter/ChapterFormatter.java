package net.therap.virtualexam.formatter;

import net.therap.virtualexam.domain.Chapter;
import net.therap.virtualexam.exception.InvalidChapterIdException;
import net.therap.virtualexam.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

/**
 * @author tanmoy.das
 * @since 6/8/20
 */
@Component
public class ChapterFormatter implements Formatter<Chapter> {

    @Autowired
    private ChapterService chapterService;

    @Override
    public Chapter parse(String idTxt, Locale locale) throws ParseException {
        long id = Long.parseLong(idTxt);

        return chapterService.find(id).orElseThrow(InvalidChapterIdException::new);
    }

    @Override
    public String print(Chapter chapter, Locale locale) {
        return String.valueOf(chapter.getId());
    }
}

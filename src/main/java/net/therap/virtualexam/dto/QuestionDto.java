package net.therap.virtualexam.dto;

import net.therap.virtualexam.domain.Question;

import java.io.Serializable;

/**
 * @author tanmoy.das
 * @since 6/29/20
 */
public class QuestionDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long questionId;
    private String chapterName;
    private String questionCode;
    private String questionTitle;

    public QuestionDto(Question question) {
        this.questionId = question.getId();
        this.chapterName = question.getChapter().getLabel();
        this.questionCode = question.getCode();
        this.questionTitle = question.getTitle();
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}

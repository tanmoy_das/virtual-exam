package net.therap.virtualexam.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author tanmoy.das
 * @since 4/12/20
 */
@MappedSuperclass
public abstract class Persistence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    protected Date created;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date updated;

    @Version
    protected long version;

    @PrePersist
    public void onCreate() {
        this.created = new Date();
        onUpdate();
    }

    @PreUpdate
    public void onUpdate() {
        this.updated = new Date();
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}

package net.therap.virtualexam.domain;

/**
 * @author tanmoy.das
 * @since 5/28/20
 */
public enum Status {

    DRAFT(11),

    APPROVED(3),

    DELETED(7);

    private final int value;

    Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

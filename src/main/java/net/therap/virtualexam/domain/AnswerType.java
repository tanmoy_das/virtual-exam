package net.therap.virtualexam.domain;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
public enum AnswerType {

    DROPDOWN("Dropdown"),

    RADIO("Radio"),

    CHECKBOX("Checkbox"),

    TEXT_FIELD("Text Field"),

    TEXT_AREA("Text Area");

    private final String data;

    private static final Map<String, AnswerType> VALUE_TYPE_MAP = Arrays.stream(AnswerType.values())
            .collect(Collectors.toMap(AnswerType::getData,
                    answerType -> answerType,
                    (e1, e2) -> e1,
                    LinkedHashMap::new));

    AnswerType(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public static AnswerType get(String data) {
        return VALUE_TYPE_MAP.get(data);
    }
}

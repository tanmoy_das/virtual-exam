package net.therap.virtualexam.domain;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import static javax.persistence.CascadeType.ALL;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@NamedQueries({
        @NamedQuery(name = "Questionnaire.findAll",
                query = "FROM Questionnaire questionnaire " +
                        "WHERE questionnaire.status != :status")
})
@Entity
@Table(name = "questionnaire")
public class Questionnaire extends Persistence {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "{error.emptyQuestionnaireLabel}")
    private String label;

    @ManyToOne(optional = false)
    @JoinColumn(name = "subject_id")
    private Subject subject;

    private String chaptersJson;

    @ManyToMany(cascade = ALL)
    @JoinTable(
            name = "questionnaire_question",
            joinColumns = {@JoinColumn(name = "questionnaire_id")},
            inverseJoinColumns = {@JoinColumn(name = "question_id")}
    )
    private List<Question> questions;

    private String questionOrderJson;

    @OneToMany(mappedBy = "questionnaire", cascade = ALL)
    private List<QuestionnaireAssignment> questionnaireAssignments;

    @ManyToOne(optional = false)
    @JoinColumn(name = "created_by_id")
    private User createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date deleted;

    @ManyToOne
    @JoinColumn(name = "deleted_by_id")
    private User deletedBy;

    @Enumerated(EnumType.STRING)
    private Status status;

    public boolean isNew() {
        return getId() == 0;
    }

    public List<String> getChapterLabels() {
        JSONArray chapterList = new JSONArray(chaptersJson);
        List<String> chapterNames = new ArrayList<>();

        IntStream.range(0, chapterList.length())
                .forEach(index -> {
                    JSONObject chapterJson = chapterList.getJSONObject(index);
                    chapterNames.add(chapterJson.getString("label"));
                });

        return chapterNames;
    }

    public List<Long> getChapterIds() {
        JSONArray chapterList = new JSONArray(chaptersJson);
        List<Long> chapterIds = new ArrayList<>();

        IntStream.range(0, chapterList.length())
                .forEach(index -> {
                    JSONObject chapterJson = chapterList.getJSONObject(index);
                    chapterIds.add(chapterJson.getLong("id"));
                });

        return chapterIds;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getChaptersJson() {
        return chaptersJson;
    }

    public void setChaptersJson(String chapters) {
        this.chaptersJson = chapters;
    }

    public String getQuestionOrderJson() {
        return questionOrderJson;
    }

    public void setQuestionOrderJson(String questionOrder) {
        this.questionOrderJson = questionOrder;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public User getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(User deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<QuestionnaireAssignment> getQuestionnaireAssignments() {
        return questionnaireAssignments;
    }

    public void setQuestionnaireAssignments(List<QuestionnaireAssignment> questionnaireAssignments) {
        this.questionnaireAssignments = questionnaireAssignments;
    }
}

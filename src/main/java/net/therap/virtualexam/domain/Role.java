package net.therap.virtualexam.domain;

/**
 * @author tanmoy.das
 * @since 5/28/20
 */
public enum Role {

    STUDENT,

    TEACHER
}

package net.therap.virtualexam.domain;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

import static net.therap.virtualexam.domain.Status.DRAFT;

/**
 * @author tanmoy.das
 * @since 4/12/20
 */
@NamedQueries({
        @NamedQuery(name = "User.findAll",
                query = "FROM User user WHERE user.status != :status"),
        @NamedQuery(name = "User.findByEmail",
                query = "FROM User user WHERE user.email = :email " +
                        "AND user.status != :status"),
        @NamedQuery(name = "User.findByEmailAndPassword",
                query = "FROM User user WHERE user.email = :email AND user.password = :password " +
                        "AND user.status != :status"),
        @NamedQuery(name = "User.findByExample",
                query = "FROM User user WHERE user.email = :email AND user.password = :password " +
                        "AND user.status != :status"),
        @NamedQuery(name = "User.findContainingName",
                query = "FROM User user WHERE user.fullName LIKE CONCAT('%',:name,'%') " +
                        "AND user.status != :status"),
        @NamedQuery(name = "User.findByRole",
                query = "FROM User user WHERE user.role = :role " +
                        "AND user.status != :status")
})
@Entity
@Table(name = "user")
public class User extends Persistence {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Size(min = 3, max = 30, message = "{name.sizeMismatch}")
    private String fullName;

    @Email
    @NotEmpty(message = "{email.notEmpty}")
    private String email;

    @Size(min = 8, message = "{password.sizeMismatch}")
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Temporal(TemporalType.TIMESTAMP)
    private Date deleted;

    @ManyToOne
    @JoinColumn(name = "deleted_by_id")
    private User deletedBy;

    @Enumerated(EnumType.STRING)
    private Status status;

    public boolean isNew() {
        return getId() == 0;
    }

    public User() {
        this.status = DRAFT;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(User deletedBy) {
        this.deletedBy = deletedBy;
    }
}

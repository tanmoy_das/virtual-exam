package net.therap.virtualexam.domain;

import org.json.JSONArray;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@NamedQueries({
        @NamedQuery(name = "Question.findAll",
                query = "FROM Question question " +
                        "WHERE question.status != :status"),
        @NamedQuery(name = "Question.findByCode",
                query = "FROM Question question where question.code = :code" +
                        " AND question.status != :status")
})
@Entity
@Table(name = "question",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"code"})
        }
)
public class Question extends Persistence {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String code;

    private String title;

    private String hint;

    @ManyToOne(optional = false)
    @JoinColumn(name = "chapter_id")
    private Chapter chapter;

    @Enumerated(EnumType.STRING)
    private AnswerType answerType;

    private String correctOptionsJson;

    @OneToMany(mappedBy = "question")
    private List<AnswerOption> options;

    @ManyToOne(optional = false)
    @JoinColumn(name = "created_by_id")
    private User createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date deleted;

    @ManyToOne
    @JoinColumn(name = "deleted_by_id")
    private User deletedBy;

    @Enumerated(EnumType.STRING)
    private Status status;

    public Question() {
        this.correctOptionsJson = new JSONArray().toString();
    }

    public boolean isWrittenQuestion() {
        return answerType == AnswerType.TEXT_FIELD || answerType == AnswerType.TEXT_AREA;
    }

    public boolean isNew() {
        return getId() == 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }

    public void setAnswerType(AnswerType answerType) {
        this.answerType = answerType;
    }

    public String getCorrectOptionsJson() {
        return correctOptionsJson;
    }

    public void setCorrectOptionsJson(String correctOptions) {
        this.correctOptionsJson = correctOptions;
    }

    public List<AnswerOption> getOptions() {
        return options;
    }

    public void setOptions(List<AnswerOption> options) {
        this.options = options;
    }

    public User getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(User deletedBy) {
        this.deletedBy = deletedBy;
    }
}

package net.therap.virtualexam.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.therap.virtualexam.command.QuestionReorderCommand;
import net.therap.virtualexam.command.QuestionnaireCommand;
import net.therap.virtualexam.domain.*;
import net.therap.virtualexam.exception.WebAccessException;
import net.therap.virtualexam.helper.QuestionnaireHelper;
import net.therap.virtualexam.service.QuestionnaireService;
import net.therap.virtualexam.service.SubjectService;
import net.therap.virtualexam.validator.QuestionnaireValidator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.helper.UrlHelper.redirectTo;
import static net.therap.virtualexam.util.Constants.*;
import static net.therap.virtualexam.util.Constants.DASHBOARD_PATH;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
@SessionAttributes({"questionnaireCommand", "questionReorderCommand"})
@RequestMapping("questionnaire")
public class QuestionnaireController {

    private static final Logger logger = LoggerFactory.getLogger(QuestionnaireController.class);

    private static final String ADD_QUESTIONNAIRE_PAGE = "addQuestionnaire";
    private static final String CHOOSE_QUESTIONS_PAGE = "chooseQuestions";
    private static final String QUESTIONNAIRE_COMMAND_LABEL = "questionnaireCommand";
    private static final String AVAILABLE_QUESTIONS_LABEL = "availableQuestions";
    private static final String REORDER_QUESTIONNAIRE_QUESTIONS_PAGE = "reorderQuestionnaireQuestions";
    private static final String QUESTION_REORDER_COMMAND_LABEL = "questionReorderCommand";

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private QuestionnaireValidator questionnaireValidator;

    @Autowired
    private QuestionnaireHelper questionnaireHelper;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(questionnaireValidator);
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String showAddQuestionnairePage(@RequestParam("subjectId") long subjectId,
                                           ModelMap model,
                                           HttpSession session) {

        checkAccess(session);

        model.put(QUESTIONNAIRE_COMMAND_LABEL, createQuestionnaireCommand(subjectId, session));

        return ADD_QUESTIONNAIRE_PAGE;
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String deleteQuestionnaire(@RequestParam("questionnaireId") Questionnaire questionnaire,
                                      ModelMap model,
                                      HttpSession session) {

        checkAccess(session);

        questionnaireService.delete(questionnaire);

        return redirectTo("/subject/show?subjectId=" + questionnaire.getSubject().getId());
    }

    @RequestMapping(value = "submit", params = "_action_generate_questionnaire", method = RequestMethod.POST)
    public String generateQuestionnaire(@Valid
                                        @ModelAttribute(QUESTIONNAIRE_COMMAND_LABEL)
                                                QuestionnaireCommand questionnaireCommand,
                                        BindingResult bindingResult,
                                        HttpSession session,
                                        SessionStatus sessionStatus) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return ADD_QUESTIONNAIRE_PAGE;
        }

        Questionnaire questionnaire = questionnaireCommand.getQuestionnaire();
        extractChaptersData(questionnaireCommand, questionnaire);

        List<Question> questions = generateQuestions(
                questionnaireCommand.getChapters(),
                questionnaireCommand.getNumberOfQuestions()
        );

        questionnaire.setQuestions(questions);
        questionnaireHelper.generateQuestionOrder(questionnaire);
        questionnaireService.approve(questionnaire);

        sessionStatus.setComplete();

        return redirectTo("/subject/show?subjectId=" + questionnaire.getSubject().getId());
    }

    @RequestMapping(value = "submit", params = "_action_choose_questions", method = RequestMethod.POST)
    public String showChooseQuestionsPage(@Valid
                                          @ModelAttribute(QUESTIONNAIRE_COMMAND_LABEL)
                                                  QuestionnaireCommand questionnaireCommand,
                                          BindingResult bindingResult,
                                          ModelMap model,
                                          HttpSession session) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return ADD_QUESTIONNAIRE_PAGE;
        }

        Questionnaire questionnaire = questionnaireCommand.getQuestionnaire();
        extractChaptersData(questionnaireCommand, questionnaire);

        List<Question> questions = new ArrayList<>();
        for (Chapter chapter : questionnaireCommand.getChapters()) {
            questions.addAll(chapter.getQuestions());
        }
        questionnaireCommand.setAvailableQuestions(questions);

        return CHOOSE_QUESTIONS_PAGE;
    }

    @RequestMapping(value = "submit", params = "_action_save_questionnaire", method = RequestMethod.POST)
    public String addQuestionnaire(@Valid
                                   @ModelAttribute(QUESTIONNAIRE_COMMAND_LABEL)
                                           QuestionnaireCommand questionnaireCommand,
                                   BindingResult bindingResult,
                                   HttpSession session,
                                   SessionStatus sessionStatus) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return CHOOSE_QUESTIONS_PAGE;
        }

        Questionnaire questionnaire = questionnaireCommand.getQuestionnaire();
        questionnaireHelper.generateQuestionOrder(questionnaire);
        questionnaireService.approve(questionnaire);

        sessionStatus.setComplete();

        return redirectTo("/subject/show?subjectId=" + questionnaire.getSubject().getId());
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String showEditQuestionnairePage(@RequestParam("questionnaireId") long questionnaireId,
                                            ModelMap model,
                                            HttpSession session) throws JsonProcessingException {

        checkAccess(session);

        Questionnaire questionnaire = questionnaireService.find(questionnaireId)
                .orElseThrow(IllegalArgumentException::new);
        questionnaireHelper.fixQuestionOrder(questionnaire);

        QuestionReorderCommand questionReorderCommand = new QuestionReorderCommand(questionnaire);
        model.put(QUESTION_REORDER_COMMAND_LABEL, questionReorderCommand);

        return REORDER_QUESTIONNAIRE_QUESTIONS_PAGE;
    }

    @RequestMapping(value = "update", params = "_action_reorder_questions", method = RequestMethod.POST)
    public String updateQuestionnaire(@Valid
                                      @ModelAttribute(QUESTION_REORDER_COMMAND_LABEL)
                                              QuestionReorderCommand questionReorderCommand,
                                      BindingResult bindingResult,
                                      HttpSession session,
                                      SessionStatus sessionStatus) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return REORDER_QUESTIONNAIRE_QUESTIONS_PAGE;
        }

        Questionnaire questionnaire = questionReorderCommand.getQuestionnaire();
        questionnaireService.approve(questionnaire);

        sessionStatus.setComplete();

        return redirectTo("/subject/show?subjectId=" + questionnaire.getSubject().getId());
    }

    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }

    private void checkAccess(HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        if (currentUser.getRole() == STUDENT) {
            throw new WebAccessException();
        }
    }

    private QuestionnaireCommand createQuestionnaireCommand(long subjectId, HttpSession session) {
        Subject subject = subjectService.find(subjectId).orElseThrow(IllegalArgumentException::new);
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);


        QuestionnaireCommand questionnaireCommand = new QuestionnaireCommand();
        questionnaireCommand.getQuestionnaire().setSubject(subject);
        questionnaireCommand.getQuestionnaire().setCreatedBy(currentUser);

        return questionnaireCommand;
    }

    private void extractChaptersData(QuestionnaireCommand questionnaireCommand,
                                     Questionnaire questionnaire) {

        JSONArray chaptersJsonArray = new JSONArray();

        for (Chapter chapter : questionnaireCommand.getChapters()) {
            JSONObject chapterJson = new JSONObject();
            chapterJson.put("id", chapter.getId());
            chapterJson.put("label", chapter.getLabel());
            chaptersJsonArray.put(chapterJson);
        }
        questionnaire.setChaptersJson(chaptersJsonArray.toString());
    }

    private List<Question> generateQuestions(List<Chapter> chapters, int numberOfQuestions) {
        List<Question> questions = new ArrayList<>();
        for (Chapter chapter : chapters) {
            questions.addAll(chapter.getQuestions());
        }

        Collections.shuffle(questions);
        return questions.subList(0, numberOfQuestions);
    }
}

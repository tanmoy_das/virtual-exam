package net.therap.virtualexam.web.controller;

import net.therap.virtualexam.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

import static net.therap.virtualexam.util.Constants.CURRENT_USER_LABEL;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
public class ProfileController {

    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

    private static final String PROFILE_PAGE = "profile";

    @RequestMapping(value = "profile", method = RequestMethod.GET)
    public String showProfilePage(ModelMap model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);

        //TODO:IMPLEMENT LOGIC
        return PROFILE_PAGE;
    }
}

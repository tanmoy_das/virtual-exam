package net.therap.virtualexam.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.therap.virtualexam.command.SubjectCommand;
import net.therap.virtualexam.domain.Questionnaire;
import net.therap.virtualexam.domain.Subject;
import net.therap.virtualexam.domain.User;
import net.therap.virtualexam.exception.WebAccessException;
import net.therap.virtualexam.helper.QuestionnaireHelper;
import net.therap.virtualexam.service.SubjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.domain.Status.APPROVED;
import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.helper.UrlHelper.redirectTo;
import static net.therap.virtualexam.util.Constants.*;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
@SessionAttributes({"subjectCommand", "currentSubject"})
@RequestMapping("subject")
public class SubjectController {

    private static final Logger logger = LoggerFactory.getLogger(SubjectController.class);


    private static final String ADD_SUBJECT_PAGE = "addSubject";
    private static final String SUBJECT_COMMAND_LABEL = "subjectCommand";
    private static final String SUBJECT_PAGE = "subject";
    private static final String CURRENT_SUBJECT_LABEL = "currentSubject";
    private static final String QUESTION_REORDER_COMMAND_LABEL = "questionReorderCommand";
    private static final String QUESTIONNAIRE_LIST_JSON_LABEL = "questionnaireListJson";


    @Autowired
    private SubjectService subjectService;

    @Autowired
    private QuestionnaireHelper questionnaireHelper;

    @Autowired
    private ObjectMapper jsonMapper;

    @RequestMapping(value = "show", method = RequestMethod.GET)
    public String showSubjectPage(@RequestParam("subjectId") long subjectId,
                                  ModelMap model,
                                  HttpSession session) {

        checkAccess(session);

        Subject subject = loadSubjectById(subjectId);
        model.put(CURRENT_SUBJECT_LABEL, subject);

        return SUBJECT_PAGE;
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String deleteSubject(@RequestParam("subjectId") Subject subject,
                                  ModelMap model,
                                  HttpSession session) {

        checkAccess(session);

        if (!canDeleteSubject(subject)) {
            return redirectTo(ERROR_PATH + "?errorCode=error.subjectHasQuestionnaire");
        }

        subjectService.delete(subject);

        return redirectTo(DASHBOARD_PATH);
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String showAddSubjectPage(ModelMap model, HttpSession session) {

        checkAccess(session);

        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        SubjectCommand subjectCommand = new SubjectCommand();
        subjectCommand.getSubject().setCreatedBy(currentUser);

        model.put(SUBJECT_COMMAND_LABEL, subjectCommand);
        return ADD_SUBJECT_PAGE;
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public String addSubject(@Valid
                             @ModelAttribute(SUBJECT_COMMAND_LABEL) SubjectCommand subjectCommand,
                             BindingResult bindingResult,
                             ModelMap model,
                             HttpSession session,
                             SessionStatus sessionStatus) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            model.put(SUBJECT_COMMAND_LABEL, subjectCommand);
            addAttributes(model);
            return ADD_SUBJECT_PAGE;
        }

        Subject subject = subjectCommand.getSubject();
        subject.setStatus(APPROVED);
        subjectService.saveOrUpdate(subject);

        sessionStatus.setComplete();

        return redirectTo(DASHBOARD_PATH);
    }

    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }

    private void checkAccess(HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);

        if (currentUser.getRole() == STUDENT) {
            throw new WebAccessException();
        }
    }

    private Subject loadSubjectById(long subjectId) {
        Subject subject = subjectService.find(subjectId).orElseThrow(IllegalArgumentException::new);
        subject.getQuestionnaires()
                .forEach(questionnaire -> questionnaireHelper.fixQuestionOrder(questionnaire));
        return subject;
    }

    private boolean canDeleteSubject(@RequestParam("subjectId") Subject subject) {
        List<Questionnaire> questionnaires = subject.getQuestionnaires().
                stream()
                .filter(questionnaire -> questionnaire.getStatus() != DELETED)
                .collect(Collectors.toList());
        return questionnaires.isEmpty();
    }
}

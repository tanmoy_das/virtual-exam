package net.therap.virtualexam.web.controller;

import net.therap.virtualexam.domain.User;
import net.therap.virtualexam.exception.WebAccessException;
import net.therap.virtualexam.service.SubjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.domain.Role.TEACHER;
import static net.therap.virtualexam.util.Constants.CURRENT_USER_LABEL;
import static net.therap.virtualexam.util.Constants.SUBJECTS_LABEL;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
public class DashboardController {

    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    private static final String DASHBOARD_PAGE = "dashboard";
    private static final String STUDENT_HOMEPAGE = "home";

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "dashboard", method = RequestMethod.GET)
    public String showDashboardPage(ModelMap model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        checkTeacherAccess(currentUser);

        return DASHBOARD_PAGE;
    }

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String showStudentHomePage(ModelMap model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        checkStudentAccess(currentUser);

        //TODO:IMPLEMENT LOGIC
        return STUDENT_HOMEPAGE;
    }

    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }

    private void checkTeacherAccess(User currentUser) {
        if (currentUser.getRole() == STUDENT) {
            throw new WebAccessException();
        }
    }

    private void checkStudentAccess(User currentUser) {
        if (currentUser.getRole() == TEACHER) {
            throw new WebAccessException();
        }
    }
}

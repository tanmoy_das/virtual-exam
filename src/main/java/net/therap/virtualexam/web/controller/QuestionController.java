package net.therap.virtualexam.web.controller;

import net.therap.virtualexam.command.QuestionCommand;
import net.therap.virtualexam.domain.*;
import net.therap.virtualexam.exception.WebAccessException;
import net.therap.virtualexam.service.ChapterService;
import net.therap.virtualexam.service.QuestionService;
import net.therap.virtualexam.service.SubjectService;
import net.therap.virtualexam.validator.QuestionValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.domain.Status.APPROVED;
import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.helper.UrlHelper.redirectTo;
import static net.therap.virtualexam.util.Constants.*;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
@SessionAttributes("questionCommand")
@RequestMapping("question")
public class QuestionController {

    private static final Logger logger = LoggerFactory.getLogger(QuestionController.class);

    private static final String ADD_QUESTION_PAGE = "addQuestion";
    private static final String QUESTION_COMMAND_LABEL = "questionCommand";

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionValidator questionValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(questionValidator);
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String displayAddQuestionPage(@RequestParam(name = "chapterId") Long chapterId,
                                         ModelMap model,
                                         HttpSession session) {

        checkAccess(session);
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        Chapter chapter = chapterService.find(chapterId).orElseThrow(IllegalArgumentException::new);

        QuestionCommand questionCommand = new QuestionCommand();
        questionCommand.getQuestion().setCreatedBy(currentUser);
        questionCommand.getQuestion().setChapter(chapter);

        model.put(QUESTION_COMMAND_LABEL, questionCommand);

        return ADD_QUESTION_PAGE;
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String deleteQuestion(@RequestParam("questionId") Question question,
                                 ModelMap model,
                                 HttpSession session) {

        checkAccess(session);

        if (!canDeleteQuestion(question)) {
            return redirectTo(ERROR_PATH + "?errorCode=error.questionUsedInQuestionnaire");
        }

        questionService.delete(question);

        Subject subject = question.getChapter().getSubject();
        return redirectTo("/subject/show?subjectId=" + subject.getId());
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public String addQuestion(@Valid @ModelAttribute(QUESTION_COMMAND_LABEL) QuestionCommand questionCommand,
                              BindingResult bindingResult,
                              ModelMap model,
                              HttpSession session,
                              SessionStatus sessionStatus) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return ADD_QUESTION_PAGE;
        }

        Question question = questionCommand.getQuestion();
        question.setStatus(APPROVED);
        questionService.saveOrUpdate(question);

        sessionStatus.setComplete();

        if (question.isWrittenQuestion()) {
            return redirectTo("/subject/show?subjectId=" + question.getChapter().getSubject().getId());
        } else {
            String url = MessageFormat.format("/answerOption/create?questionId={0}", question.getId());
            return redirectTo(url);
        }
    }

    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }

    private void checkAccess(HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        if (currentUser.getRole() == STUDENT) {
            throw new WebAccessException();
        }
    }

    private boolean canDeleteQuestion(Question question) {
        Subject subject = question.getChapter().getSubject();

        List<Questionnaire> questionnaires = subject.getQuestionnaires().
                stream()
                .filter(questionnaire -> questionnaire.getStatus() != DELETED)
                .filter(questionnaire -> containsQuestion(questionnaire, question)
                )
                .collect(Collectors.toList());

        return questionnaires.isEmpty();
    }

    private boolean containsQuestion(Questionnaire questionnaire, Question question) {
        return questionnaire.getQuestions()
                .stream()
                .map(Question::getId)
                .collect(Collectors.toList())
                .contains(question.getId());
    }
}

package net.therap.virtualexam.web.controller;

import net.therap.virtualexam.service.SubjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static net.therap.virtualexam.util.Constants.SUBJECTS_LABEL;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
@RequestMapping("error")
public class ErrorController {

    private static final Logger logger = LoggerFactory.getLogger(ErrorController.class);
    private static final String ERROR_PAGE = "error";

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String showAddQuestionnairePage() {
        logger.error("Unexpected Error Occurred");

        return ERROR_PAGE;
    }

    @RequestMapping(value = {"", "/"}, params = "errorCode", method = RequestMethod.GET)
    public String showAddQuestionnairePageWithError(@RequestParam("errorCode") String errorCode,
                                                    ModelMap model) {

        logger.error("Unexpected Error Occurred");
        model.put("errorCode", errorCode);

        return ERROR_PAGE;
    }


    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }
}

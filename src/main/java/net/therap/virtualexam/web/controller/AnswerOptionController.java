package net.therap.virtualexam.web.controller;

import net.therap.virtualexam.command.AnswerOptionCommand;
import net.therap.virtualexam.domain.AnswerOption;
import net.therap.virtualexam.domain.Question;
import net.therap.virtualexam.domain.User;
import net.therap.virtualexam.exception.WebAccessException;
import net.therap.virtualexam.service.AnswerOptionService;
import net.therap.virtualexam.service.QuestionService;
import net.therap.virtualexam.service.SubjectService;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.MessageFormat;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.helper.UrlHelper.redirectTo;
import static net.therap.virtualexam.util.Constants.CURRENT_USER_LABEL;
import static net.therap.virtualexam.util.Constants.SUBJECTS_LABEL;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
@SessionAttributes("answerOptionCommand")
@RequestMapping("answerOption")
public class AnswerOptionController {

    private static final Logger logger = LoggerFactory.getLogger(AnswerOptionController.class);

    private static final String ADD_ANSWER_OPTION_PAGE = "addAnswerOption";
    private static final String ANSWER_OPTION_COMMAND_LABEL = "answerOptionCommand";

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private AnswerOptionService answerOptionService;

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String showAddAnswerOptionPage(@RequestParam("questionId") long questionId,
                                          ModelMap model,
                                          HttpSession session) {

        checkAccess(session);

        model.put(ANSWER_OPTION_COMMAND_LABEL, createAnswerOptionCommand(questionId, session));

        return ADD_ANSWER_OPTION_PAGE;
    }

    @RequestMapping(value = "submit", params = "_action_save", method = RequestMethod.POST)
    public String addAnswerOption(@Valid
                                  @ModelAttribute(ANSWER_OPTION_COMMAND_LABEL) AnswerOptionCommand answerOptionCommand,
                                  BindingResult bindingResult,
                                  HttpSession session,
                                  SessionStatus sessionStatus) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return ADD_ANSWER_OPTION_PAGE;
        }

        approveAnswerOption(answerOptionCommand);
        sessionStatus.setComplete();

        Question question = questionService.find(answerOptionCommand.getAnswerOption().getQuestion().getId())
                .orElseThrow(IllegalArgumentException::new);

        return redirectTo("/subject/show?subjectId=" + question.getChapter().getSubject().getId());
    }

    @RequestMapping(value = "submit", params = "_action_save_and_add", method = RequestMethod.POST)
    public String addAnswerOptionAndRepeat(@Valid
                                           @ModelAttribute(ANSWER_OPTION_COMMAND_LABEL) AnswerOptionCommand answerOptionCommand,
                                           BindingResult bindingResult,
                                           HttpSession session,
                                           SessionStatus sessionStatus) {


        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return ADD_ANSWER_OPTION_PAGE;
        }

        approveAnswerOption(answerOptionCommand);
        sessionStatus.setComplete();

        Question question = questionService.find(answerOptionCommand.getAnswerOption().getQuestion().getId())
                .orElseThrow(IllegalArgumentException::new);

        String url = MessageFormat.format("/answerOption/create?questionId={0}", question.getId());
        return redirectTo(url);
    }

    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }

    private void checkAccess(HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);

        if (currentUser.getRole() == STUDENT) {
            throw new WebAccessException();
        }
    }

    private void approveAnswerOption(AnswerOptionCommand answerOptionCommand) {
        AnswerOption answerOption = answerOptionCommand.getAnswerOption();
        Question question = questionService.find(answerOption.getQuestion().getId())
                .orElseThrow(IllegalArgumentException::new);

        answerOptionService.approve(answerOption);

        if (answerOptionCommand.getAnswerOption().isCorrect()) {
            long answerOptionId = answerOption.getId();

            JSONArray correctAnswers = new JSONArray(question.getCorrectOptionsJson());
            correctAnswers.put(answerOptionId);
            question.setCorrectOptionsJson(correctAnswers.toString());
            questionService.saveOrUpdate(question);
        }
    }

    private AnswerOptionCommand createAnswerOptionCommand(long questionId, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        Question question = questionService.find(questionId).orElseThrow(IllegalArgumentException::new);

        AnswerOptionCommand answerOptionCommand = new AnswerOptionCommand();
        answerOptionCommand.getAnswerOption().setQuestion(question);
        answerOptionCommand.getAnswerOption().setCreatedBy(currentUser);

        return answerOptionCommand;
    }
}

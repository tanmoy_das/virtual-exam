package net.therap.virtualexam.web.controller;

import net.therap.virtualexam.command.ChapterCommand;
import net.therap.virtualexam.domain.Chapter;
import net.therap.virtualexam.domain.Questionnaire;
import net.therap.virtualexam.domain.Subject;
import net.therap.virtualexam.domain.User;
import net.therap.virtualexam.exception.WebAccessException;
import net.therap.virtualexam.service.ChapterService;
import net.therap.virtualexam.service.SubjectService;
import net.therap.virtualexam.validator.ChapterValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.domain.Status.APPROVED;
import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.helper.UrlHelper.redirectTo;
import static net.therap.virtualexam.util.Constants.*;
import static net.therap.virtualexam.util.Constants.DASHBOARD_PATH;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
@SessionAttributes({"chapterCommand", "currentSubject"})
@RequestMapping("chapter")
public class ChapterController {

    private static final Logger logger = LoggerFactory.getLogger(ChapterController.class);

    private static final String ADD_CHAPTER_PAGE = "addChapter";
    private static final String CHAPTER_COMMAND_LABEL = "chapterCommand";
    private static final String CURRENT_SUBJECT_LABEL = "currentSubject";
    private static final int NO_SUBJECT_SELECTED = 0;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private ChapterValidator chapterValidator;

    @InitBinder(CHAPTER_COMMAND_LABEL)
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(chapterValidator);
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String showAddChapterPage(@RequestParam(name = "subjectId") long subjectId,
                                     ModelMap model,
                                     HttpSession session) {

        checkAccess(session);

        Subject subject = null;
        if (subjectId != NO_SUBJECT_SELECTED) {
            subject = subjectService.find(subjectId).orElseThrow(IllegalArgumentException::new);
        }
        model.put(CURRENT_SUBJECT_LABEL, subject);

        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
        
        ChapterCommand chapterCommand = new ChapterCommand();
        chapterCommand.getChapter().setCreatedBy(currentUser);
        chapterCommand.getChapter().setSubject(subject);
        
        model.put(CHAPTER_COMMAND_LABEL, chapterCommand);

        return ADD_CHAPTER_PAGE;
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public String addChapter(@Valid
                             @ModelAttribute(CHAPTER_COMMAND_LABEL) ChapterCommand chapterCommand,
                             BindingResult bindingResult,
                             ModelMap model,
                             HttpSession session,
                             SessionStatus sessionStatus) {
        
        checkAccess(session);

        if (bindingResult.hasErrors()) {
            model.put(CHAPTER_COMMAND_LABEL, chapterCommand);
            addAttributes(model);
            return ADD_CHAPTER_PAGE;
        }

        Chapter chapter = chapterCommand.getChapter();
        chapter.setStatus(APPROVED);
        chapterService.saveOrUpdate(chapter);

        sessionStatus.setComplete();

        return redirectTo("/subject/show?subjectId=" + chapter.getSubject().getId());
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String deleteChapter(@RequestParam("chapterId") Chapter chapter,
                                ModelMap model,
                                HttpSession session) {

        checkAccess(session);

        if (!canDeleteChapter(chapter)) {
            return redirectTo(ERROR_PATH + "?errorCode=error.chapterUsedInQuestionnaire");
        }

        chapterService.delete(chapter);

        Subject subject = chapter.getSubject();
        return redirectTo("/subject/show?subjectId=" + subject.getId());
    }

    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }

    private void checkAccess(HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);

        if (currentUser.getRole() == STUDENT) {
            throw new WebAccessException();
        }
    }

    private boolean canDeleteChapter(Chapter chapter) {
        Subject subject = chapter.getSubject();

        List<Questionnaire> questionnaires = subject.getQuestionnaires().
                stream()
                .filter(questionnaire -> questionnaire.getStatus() != DELETED)
                .filter(questionnaire -> questionnaire.getChapterIds().contains(chapter.getId()))
                .collect(Collectors.toList());

        return questionnaires.isEmpty();
    }
}

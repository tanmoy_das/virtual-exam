package net.therap.virtualexam.web.controller;

import net.therap.virtualexam.command.QuestionnaireAssignmentCommand;
import net.therap.virtualexam.domain.Questionnaire;
import net.therap.virtualexam.domain.QuestionnaireAssignment;
import net.therap.virtualexam.domain.User;
import net.therap.virtualexam.exception.WebAccessException;
import net.therap.virtualexam.service.QuestionnaireAssignmentService;
import net.therap.virtualexam.service.QuestionnaireService;
import net.therap.virtualexam.service.SubjectService;
import net.therap.virtualexam.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.helper.UrlHelper.redirectTo;
import static net.therap.virtualexam.util.Constants.CURRENT_USER_LABEL;
import static net.therap.virtualexam.util.Constants.SUBJECTS_LABEL;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
@SessionAttributes({"questionnaireAssignmentCommand", "currentQuestionnaire", "students"})
@RequestMapping("questionnaireAssignment")
public class QuestionnaireAssignmentController {

    private static final Logger logger = LoggerFactory.getLogger(QuestionnaireAssignmentController.class);

    private static final String QUESTIONNAIRE_ASSIGNMENT_PAGE = "questionnaireAssignment";
    private static final String CURRENT_QUESTIONNAIRE_LABEL = "currentQuestionnaire";
    private static final String QUESTIONNAIRE_ASSIGNMENT_COMMAND_LABEL = "questionnaireAssignmentCommand";
    private static final String STUDENTS_LABEL = "students";

    @Autowired
    private QuestionnaireAssignmentService questionnaireAssignmentService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private UserService userService;

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String showIndexPage(@RequestParam("questionnaireId") long questionnaireId,
                                ModelMap model,
                                HttpSession session) {

        checkAccess(session);

        Questionnaire questionnaire = questionnaireService.find(questionnaireId)
                .orElseThrow(IllegalArgumentException::new);
        model.put(CURRENT_QUESTIONNAIRE_LABEL, questionnaire);

        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);

        QuestionnaireAssignmentCommand questionnaireAssignmentCommand = new QuestionnaireAssignmentCommand();
        questionnaireAssignmentCommand.getQuestionnaireAssignment().setQuestionnaire(questionnaire);
        questionnaireAssignmentCommand.getQuestionnaireAssignment().setCreatedBy(currentUser);
        model.put(QUESTIONNAIRE_ASSIGNMENT_COMMAND_LABEL, questionnaireAssignmentCommand);

        List<User> students = userService.findAllStudents();
        model.put(STUDENTS_LABEL, students);

        return QUESTIONNAIRE_ASSIGNMENT_PAGE;
    }

//    @RequestMapping(value = "addQuestionnaireAssignment", method = RequestMethod.GET)
//    public String showAddQuestionnaireAssignmentPage(ModelMap model, HttpSession session) {
//        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);
//        checkAccess(session);
//
//        QuestionnaireAssignmentCommand questionnaireAssignmentCommand = new QuestionnaireAssignmentCommand();
//        questionnaireAssignmentCommand.getQuestionnaireAssignment().setCreatedBy(currentUser);
//
//        model.put(SUBJECT_COMMAND_LABEL, questionnaireAssignmentCommand);
//        return ADD_SUBJECT_PAGE;
//    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public String addQuestionnaireAssignment(@Valid
                                             @ModelAttribute(QUESTIONNAIRE_ASSIGNMENT_COMMAND_LABEL)
                                                     QuestionnaireAssignmentCommand questionnaireAssignmentCommand,
                                             BindingResult bindingResult,
                                             ModelMap model,
                                             HttpSession session,
                                             SessionStatus sessionStatus) {

        checkAccess(session);

        if (bindingResult.hasErrors()) {
            return QUESTIONNAIRE_ASSIGNMENT_PAGE;
        }

        QuestionnaireAssignment questionnaireAssignment = questionnaireAssignmentCommand.getQuestionnaireAssignment();
        Questionnaire questionnaire = questionnaireAssignment.getQuestionnaire();

        questionnaireAssignmentService.approve(questionnaireAssignment);
        questionnaire.getQuestionnaireAssignments().add(questionnaireAssignment);
        questionnaireService.saveOrUpdate(questionnaire);

        sessionStatus.setComplete();

        String questionnaireAssignmentUrl = "/questionnaireAssignment/index?questionnaireId=" +
                questionnaireAssignment.getQuestionnaire().getId();
        return redirectTo(questionnaireAssignmentUrl);
    }

    @ModelAttribute
    public void addAttributes(ModelMap model) {
        model.put(SUBJECTS_LABEL, subjectService.findAll());
    }

    private void checkAccess(HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER_LABEL);

        if (currentUser.getRole() == STUDENT) {
            throw new WebAccessException();
        }
    }
}

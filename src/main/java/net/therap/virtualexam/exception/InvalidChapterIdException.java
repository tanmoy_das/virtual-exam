package net.therap.virtualexam.exception;

import java.text.ParseException;

public class InvalidChapterIdException extends ParseException {

    public InvalidChapterIdException() {
        super("No Chapter exists for this chapter id", 0);
    }
}

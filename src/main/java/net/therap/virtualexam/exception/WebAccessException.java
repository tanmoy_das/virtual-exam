package net.therap.virtualexam.exception;

/**
 * @author tanmoy.das
 * @since 6/8/20
 */
public class WebAccessException extends RuntimeException {
    public WebAccessException() {
        super("User does not have necessary privileges");
    }
}

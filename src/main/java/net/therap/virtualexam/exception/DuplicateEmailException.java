package net.therap.virtualexam.exception;

public class DuplicateEmailException extends InvalidUserException {

    public DuplicateEmailException() {
        super("Email Already Exists");
    }
}

package net.therap.virtualexam.exception;

import java.text.ParseException;

public class InvalidSubjectIdException extends ParseException {

    public InvalidSubjectIdException() {
        super("No Subject exists for this subject id", 0);
    }
}

package net.therap.virtualexam.service;

import net.therap.virtualexam.domain.QuestionnaireAssignment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static net.therap.virtualexam.domain.Status.APPROVED;
import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.util.Constants.PERSISTENCE_UNIT_NAME;
import static net.therap.virtualexam.util.Constants.STATUS_LABEL;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@Repository
public class QuestionnaireAssignmentService implements Dao<QuestionnaireAssignment> {

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Override
    public Optional<QuestionnaireAssignment> find(long id) {
        QuestionnaireAssignment item = em.find(QuestionnaireAssignment.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public Optional<QuestionnaireAssignment> find(QuestionnaireAssignment item) {
        return find(item.getId());
    }

    @Override
    public List<QuestionnaireAssignment> findAll() {
        String queryName = "QuestionnaireAssignment.findAll";
        return em.createNamedQuery(queryName, QuestionnaireAssignment.class)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Override
    @Transactional
    public QuestionnaireAssignment saveOrUpdate(QuestionnaireAssignment item) {
        QuestionnaireAssignment persistedItem;

        if (item.isNew()) {
            em.persist(item);
            persistedItem = item;
        } else {
            persistedItem = em.merge(item);
        }
        em.flush();

        return persistedItem;
    }

    @Override
    @Transactional
    public void destroy(QuestionnaireAssignment item) {
        find(item).ifPresent(persistedItem -> {
            em.remove(persistedItem);
            em.flush();
        });
    }

    @Transactional
    public void approve(QuestionnaireAssignment questionnaireAssignment) {
        questionnaireAssignment.setStatus(APPROVED);
        saveOrUpdate(questionnaireAssignment);
    }
}

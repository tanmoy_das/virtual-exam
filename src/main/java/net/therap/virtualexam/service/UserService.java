package net.therap.virtualexam.service;

import net.therap.virtualexam.domain.User;
import net.therap.virtualexam.util.HashingUtil;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.therap.virtualexam.domain.Role.STUDENT;
import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.util.Constants.PERSISTENCE_UNIT_NAME;
import static net.therap.virtualexam.util.Constants.STATUS_LABEL;

/**
 * @author tanmoy.das
 * @since 4/24/20
 */
@Repository
public class UserService implements Dao<User> {

    public static final String EMAIL_LABEL = "email";
    public static final String PASSWORD_LABEL = "password";

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Override
    public Optional<User> find(User user) {
        return em.createNamedQuery("User.findByExample", User.class)
                .setParameter(EMAIL_LABEL, user.getEmail())
                .setParameter(PASSWORD_LABEL, user.getPassword())
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList()
                .stream()
                .findFirst();
    }

    public Optional<User> findUserByEmail(String email) {
        return em.createNamedQuery("User.findByEmail", User.class)
                .setParameter(EMAIL_LABEL, email)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList()
                .stream()
                .findFirst();
    }

    public Optional<User> findUserByEmailAndPassword(String email, String password) throws NoSuchAlgorithmException {
        String hashedPassword = HashingUtil.sha256Hash(password);

        return em.createNamedQuery("User.findByEmailAndPassword", User.class)
                .setParameter(EMAIL_LABEL, email)
                .setParameter(PASSWORD_LABEL, hashedPassword)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList()
                .stream()
                .findFirst();
    }

    public List<User> findUsersContainingName(String name) {
        return em.createNamedQuery("User.findContainingName", User.class)
                .setParameter("name", name)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Transactional
    public User createOrUpdateUser(User user) throws NoSuchAlgorithmException {
        user.setPassword(HashingUtil.sha256Hash(user.getPassword()));
        return saveOrUpdate(user);
    }

    public List<User> findUsersByNameOrEmail(String s) {
        List<User> users = findUsersContainingName(s);

        findUserByEmail(s).ifPresent(users::add);

        return users.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public Optional<User> find(long id) {
        User item = em.find(User.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public List<User> findAll() {
        String queryName = User.class.getSimpleName() + ".findAll";
        return em.createNamedQuery(queryName, User.class)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Override
    @Transactional
    public User saveOrUpdate(User item) {
        User persistedItem;

        if (item.isNew()) {
            em.persist(item);
            persistedItem = item;
        } else {
            persistedItem = em.merge(item);
        }
        em.flush();

        return persistedItem;
    }

    @Override
    @Transactional
    public void destroy(User item) {
        find(item).ifPresent(persistedItem -> {
            em.remove(persistedItem);
            em.flush();
        });
    }

    public List<User> findAllStudents() {
        return em.createNamedQuery("User.findByRole", User.class)
                .setParameter("role", STUDENT)
                .setParameter("status", DELETED)
                .getResultList();
    }
}

package net.therap.virtualexam.service;

import net.therap.virtualexam.domain.Chapter;
import net.therap.virtualexam.domain.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.util.Constants.PERSISTENCE_UNIT_NAME;
import static net.therap.virtualexam.util.Constants.STATUS_LABEL;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@Repository
public class SubjectService implements Dao<Subject> {

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Autowired
    private ChapterService chapterService;

    @Override
    public Optional<Subject> find(long id) {
        Subject item = em.find(Subject.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public Optional<Subject> find(Subject item) {
        return find(item.getId());
    }

    @Override
    public List<Subject> findAll() {
        String queryName = "Subject.findAll";
        return em.createNamedQuery(queryName, Subject.class)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Override
    @Transactional
    public Subject saveOrUpdate(Subject item) {
        Subject persistedItem;

        if (item.isNew()) {
            em.persist(item);
            persistedItem = item;
        } else {
            persistedItem = em.merge(item);
        }
        em.flush();

        return persistedItem;
    }

    @Transactional
    public void delete(Subject subject) {
        for (Chapter chapter : subject.getChapters()) {
            chapterService.delete(chapter);
        }
        subject.setStatus(DELETED);
        saveOrUpdate(subject);
    }

    @Override
    @Transactional
    public void destroy(Subject item) {
        find(item).ifPresent(persistedItem -> {
            em.remove(persistedItem);
            em.flush();
        });
    }
}

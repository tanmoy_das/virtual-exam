package net.therap.virtualexam.service;

import net.therap.virtualexam.domain.Questionnaire;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static net.therap.virtualexam.domain.Status.APPROVED;
import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.util.Constants.PERSISTENCE_UNIT_NAME;
import static net.therap.virtualexam.util.Constants.STATUS_LABEL;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@Repository
public class QuestionnaireService implements Dao<Questionnaire> {

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Override
    public Optional<Questionnaire> find(long id) {
        Questionnaire item = em.find(Questionnaire.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public Optional<Questionnaire> find(Questionnaire item) {
        return find(item.getId());
    }

    @Override
    public List<Questionnaire> findAll() {
        String queryName = "Questionnaire.findAll";
        return em.createNamedQuery(queryName, Questionnaire.class)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Override
    @Transactional
    public Questionnaire saveOrUpdate(Questionnaire item) {
        Questionnaire persistedItem;

        if (item.isNew()) {
            em.persist(item);
            persistedItem = item;
        } else {
            persistedItem = em.merge(item);
        }
        em.flush();

        return persistedItem;
    }

    @Override
    @Transactional
    public void destroy(Questionnaire item) {
        find(item).ifPresent(persistedItem -> {
            em.remove(persistedItem);
            em.flush();
        });
    }

    @Transactional
    public void approve(Questionnaire questionnaire) {
        questionnaire.setStatus(APPROVED);
        saveOrUpdate(questionnaire);
    }

    @Transactional
    public void delete(Questionnaire questionnaire) {
        questionnaire.setStatus(DELETED);
        saveOrUpdate(questionnaire);
    }
}

package net.therap.virtualexam.service;

import net.therap.virtualexam.domain.Chapter;
import net.therap.virtualexam.domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.util.Constants.PERSISTENCE_UNIT_NAME;
import static net.therap.virtualexam.util.Constants.STATUS_LABEL;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@Repository
public class ChapterService implements Dao<Chapter> {

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Autowired
    private QuestionService questionService;

    @Override
    public Optional<Chapter> find(long id) {
        Chapter item = em.find(Chapter.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public Optional<Chapter> find(Chapter item) {
        return find(item.getId());
    }

    @Override
    public List<Chapter> findAll() {
        String queryName = "Chapter.findAll";
        return em.createNamedQuery(queryName, Chapter.class)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Override
    @Transactional
    public Chapter saveOrUpdate(Chapter item) {
        Chapter persistedItem;

        if (item.isNew()) {
            em.persist(item);
            persistedItem = item;
        } else {
            persistedItem = em.merge(item);
        }
        em.flush();

        return persistedItem;
    }

    @Override
    @Transactional
    public void destroy(Chapter item) {
        find(item).ifPresent(persistedItem -> {
            em.remove(persistedItem);
            em.flush();
        });
    }

    @Transactional
    public void delete(Chapter chapter) {
        for (Question question : chapter.getQuestions()) {
            questionService.delete(question);
        }

        chapter.setStatus(DELETED);
        saveOrUpdate(chapter);
    }
}

package net.therap.virtualexam.service;

import net.therap.virtualexam.domain.AnswerOption;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static net.therap.virtualexam.domain.Status.APPROVED;
import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.util.Constants.PERSISTENCE_UNIT_NAME;
import static net.therap.virtualexam.util.Constants.STATUS_LABEL;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@Repository
public class AnswerOptionService implements Dao<AnswerOption> {

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Override
    public Optional<AnswerOption> find(long id) {
        AnswerOption item = em.find(AnswerOption.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public Optional<AnswerOption> find(AnswerOption item) {
        return find(item.getId());
    }

    @Override
    public List<AnswerOption> findAll() {
        String queryName = "AnswerOption.findAll";
        return em.createNamedQuery(queryName, AnswerOption.class)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Override
    @Transactional
    public AnswerOption saveOrUpdate(AnswerOption item) {
        AnswerOption persistedItem;

        if (item.isNew()) {
            em.persist(item);
            persistedItem = item;
        } else {
            persistedItem = em.merge(item);
        }
        em.flush();

        return persistedItem;
    }

    @Override
    @Transactional
    public void destroy(AnswerOption item) {
        find(item).ifPresent(persistedItem -> {
            em.remove(persistedItem);
            em.flush();
        });
    }

    @Transactional
    public void approve(AnswerOption answerOption) {
        answerOption.setStatus(APPROVED);
        saveOrUpdate(answerOption);
    }

    @Transactional
    public void delete(AnswerOption answerOption) {
        answerOption.setStatus(DELETED);
        saveOrUpdate(answerOption);
    }
}

package net.therap.virtualexam.service;

import net.therap.virtualexam.domain.AnswerOption;
import net.therap.virtualexam.domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static net.therap.virtualexam.domain.Status.DELETED;
import static net.therap.virtualexam.util.Constants.*;

/**
 * @author tanmoy.das
 * @since 6/1/20
 */
@Repository
public class QuestionService implements Dao<Question> {

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Autowired
    private AnswerOptionService answerOptionService;

    @Override
    public Optional<Question> find(long id) {
        Question item = em.find(Question.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public Optional<Question> find(Question item) {
        return find(item.getId());
    }

    @Override
    public List<Question> findAll() {
        String queryName = "Question.findAll";
        return em.createNamedQuery(queryName, Question.class)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList();
    }

    @Override
    @Transactional
    public Question saveOrUpdate(Question item) {
        Question persistedItem;

        if (item.isNew()) {
            em.persist(item);
            persistedItem = item;
        } else {
            persistedItem = em.merge(item);
        }
        em.flush();

        return persistedItem;
    }

    @Override
    @Transactional
    public void destroy(Question item) {
        find(item).ifPresent(persistedItem -> {
            em.remove(persistedItem);
            em.flush();
        });
    }

    public Optional<Question> findByCode(String code) {
        String queryName = "Question.findByCode";
        return em.createNamedQuery(queryName, Question.class)
                .setParameter(CODE_LABEL, code)
                .setParameter(STATUS_LABEL, DELETED)
                .getResultList()
                .stream()
                .findFirst();
    }

    @Transactional
    public void delete(Question question) {
        for (AnswerOption answerOption : question.getOptions()) {
            answerOptionService.delete(answerOption);
        }

        question.setStatus(DELETED);
        saveOrUpdate(question);
    }
}

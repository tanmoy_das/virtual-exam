package net.therap.virtualexam.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.therap.virtualexam.domain.Question;
import net.therap.virtualexam.domain.Questionnaire;
import net.therap.virtualexam.dto.QuestionDto;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 6/7/20
 */
public class QuestionReorderCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private Questionnaire questionnaire;

    private String questionDtoList;

    public QuestionReorderCommand() {
    }

    public QuestionReorderCommand(Questionnaire questionnaire) throws JsonProcessingException {
        this.questionnaire = questionnaire;
        extractQuestionDtos();
    }

    private void extractQuestionDtos() throws JsonProcessingException {
        ObjectMapper jsonObjectMapper = new ObjectMapper();

        List<QuestionDto> questionDtos = new ArrayList<>();
        for ( Question question : this.questionnaire.getQuestions() ) {
            questionDtos.add(new QuestionDto(question));
        }

        this.questionDtoList = jsonObjectMapper.writeValueAsString(questionDtos);
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) throws JsonProcessingException {
        this.questionnaire = questionnaire;
        extractQuestionDtos();
    }

    public String getQuestionDtoList() {
        return questionDtoList;
    }

    public void setQuestionDtoList(String questionDtoList) {
        this.questionDtoList = questionDtoList;
    }
}

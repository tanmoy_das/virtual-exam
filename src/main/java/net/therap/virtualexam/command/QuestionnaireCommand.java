package net.therap.virtualexam.command;

import net.therap.virtualexam.domain.Chapter;
import net.therap.virtualexam.domain.Question;
import net.therap.virtualexam.domain.Questionnaire;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static net.therap.virtualexam.domain.Status.DRAFT;

/**
 * @author tanmoy.das
 * @since 6/7/20
 */
public class QuestionnaireCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private Questionnaire questionnaire;

    @NotEmpty(message = "{error.emptyQuestionnaireChapters}")
    private List<Chapter> chapters;

    @Range(min = 1, message = "{error.insufficientQuestions}")
    private int numberOfQuestions;

    private List<Question> availableQuestions;

    private boolean completed;

    public QuestionnaireCommand() {
        this.questionnaire = new Questionnaire();
        this.chapters = new ArrayList<>();
        this.availableQuestions = new ArrayList<>();
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public List<Question> getAvailableQuestions() {
        return availableQuestions;
    }

    public void setAvailableQuestions(List<Question> availableQuestions) {
        this.availableQuestions = availableQuestions;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}

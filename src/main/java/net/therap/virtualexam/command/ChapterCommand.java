package net.therap.virtualexam.command;

import net.therap.virtualexam.domain.Chapter;

import javax.validation.Valid;
import java.io.Serializable;

import static net.therap.virtualexam.domain.Status.DRAFT;

/**
 * @author tanmoy.das
 * @since 6/7/20
 */
public class ChapterCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private Chapter chapter;

    public ChapterCommand() {
        this.chapter = new Chapter();
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }
}

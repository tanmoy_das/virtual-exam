package net.therap.virtualexam.command;

import net.therap.virtualexam.domain.Subject;

import javax.validation.Valid;
import java.io.Serializable;

import static net.therap.virtualexam.domain.Status.DRAFT;

/**
 * @author tanmoy.das
 * @since 6/7/20
 */
public class SubjectCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private Subject subject;

    public SubjectCommand() {
        this.subject = new Subject();
    }

    public SubjectCommand(Subject subject) {
        this.subject = subject;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}

package net.therap.virtualexam.command;

import net.therap.virtualexam.domain.Question;

import javax.validation.Valid;
import java.io.Serializable;

import static net.therap.virtualexam.domain.Status.DRAFT;

/**
 * @author tanmoy.das
 * @since 6/7/20
 */
public class QuestionCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private Question question;

    public QuestionCommand() {
        this.question = new Question();
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}

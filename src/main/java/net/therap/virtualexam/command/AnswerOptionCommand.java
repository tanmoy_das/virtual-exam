package net.therap.virtualexam.command;

import net.therap.virtualexam.domain.AnswerOption;

import javax.validation.Valid;
import java.io.Serializable;

import static net.therap.virtualexam.domain.Status.DRAFT;

/**
 * @author tanmoy.das
 * @since 6/7/20
 */
public class AnswerOptionCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private AnswerOption answerOption;

    public AnswerOptionCommand() {
        this.answerOption = new AnswerOption();
    }

    public AnswerOption getAnswerOption() {
        return answerOption;
    }

    public void setAnswerOption(AnswerOption answerOption) {
        this.answerOption = answerOption;
    }

}

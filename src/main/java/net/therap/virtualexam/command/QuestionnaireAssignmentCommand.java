package net.therap.virtualexam.command;

import net.therap.virtualexam.domain.QuestionnaireAssignment;

import javax.validation.Valid;
import java.io.Serializable;

import static net.therap.virtualexam.domain.Status.DRAFT;

/**
 * @author tanmoy.das
 * @since 6/7/20
 */
public class QuestionnaireAssignmentCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private QuestionnaireAssignment questionnaireAssignment;

    public QuestionnaireAssignmentCommand() {
        this.questionnaireAssignment = new QuestionnaireAssignment();
    }

    public QuestionnaireAssignmentCommand(QuestionnaireAssignment questionnaireAssignment) {
        this.questionnaireAssignment = questionnaireAssignment;
    }

    public QuestionnaireAssignment getQuestionnaireAssignment() {
        return questionnaireAssignment;
    }

    public void setQuestionnaireAssignment(QuestionnaireAssignment questionnaireAssignment) {
        this.questionnaireAssignment = questionnaireAssignment;
    }
}

package net.therap.virtualexam.helper;

import net.therap.virtualexam.domain.Question;
import net.therap.virtualexam.domain.Questionnaire;
import org.json.JSONArray;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 6/17/20
 */
@Component
public class QuestionnaireHelper {

    public void fixQuestionOrder(Questionnaire questionnaire) {
        JSONArray questionOrder = new JSONArray(questionnaire.getQuestionOrderJson());
        List<Long> orderedQuestionIds = extractOrderedQuestionIds(questionOrder);

        questionnaire.getQuestions().sort(Comparator.comparing(question -> orderedQuestionIds.indexOf(question.getId())));
    }

    private List<Long> extractOrderedQuestionIds(JSONArray questionOrder) {
        List<Long> orderedQuestionIds = new ArrayList<>();
        for (int i = 0; i < questionOrder.length(); i++) {
            orderedQuestionIds.add(questionOrder.getLong(i));
        }
        return orderedQuestionIds;
    }

    public void generateQuestionOrder(Questionnaire questionnaire) {
        JSONArray questionOrder = new JSONArray();
        for (Question question : questionnaire.getQuestions()) {
            questionOrder.put(question.getId());
        }

        questionnaire.setQuestionOrderJson(questionOrder.toString());
    }
}

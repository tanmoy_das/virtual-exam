package net.therap.virtualexam.helper;

/**
 * @author tanmoy.das
 * @since 5/4/20
 */
public class UrlHelper {

    public static String redirectTo(String url) {
        return "redirect:" + url;
    }

}

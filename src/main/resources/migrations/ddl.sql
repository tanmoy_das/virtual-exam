CREATE TABLE `user`
(
    `id`            BIGINT PRIMARY KEY AUTO_INCREMENT,
    `full_name`     varchar(255),
    `email`         varchar(255),
    `password`      varchar(255),
    `role`          varchar(64),
    `status`        ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`       TIMESTAMP                           NULL,
    `deleted_by_id` BIGINT                              NULL,
    `version`       BIGINT                              NOT NULL DEFAULT 0
);

CREATE TABLE `subject`
(
    `id`            BIGINT PRIMARY KEY AUTO_INCREMENT,
    `label`         varchar(255),
    `created_by_id` BIGINT,
    `status`        ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`       TIMESTAMP                           NULL,
    `deleted_by_id` BIGINT                              NULL,
    `version`       BIGINT                              NOT NULL DEFAULT 0
);

CREATE TABLE `questionnaire_assignment`
(
    `id`               BIGINT PRIMARY KEY AUTO_INCREMENT,
    `assignee_id`      BIGINT,
    `questionnaire_id` BIGINT,
    `created_by_id`    BIGINT,
    `status`           ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`          TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`          TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`          TIMESTAMP                           NULL,
    `deleted_by_id`    BIGINT                              NULL,
    `version`          BIGINT                              NOT NULL DEFAULT 0
);

CREATE TABLE `chapter`
(
    `id`            BIGINT PRIMARY KEY AUTO_INCREMENT,
    `label`         varchar(255),
    `subject_id`    BIGINT,
    `created_by_id` BIGINT,
    `status`        ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`       TIMESTAMP                           NULL,
    `deleted_by_id` BIGINT                              NULL,
    `version`       BIGINT                              NOT NULL DEFAULT 0
);

CREATE TABLE `question`
(
    `id`                   BIGINT PRIMARY KEY AUTO_INCREMENT,
    `code`                 varchar(255),
    `title`                varchar(255),
    `hint`                 TEXT,
    `chapter_id`           BIGINT,
    `answer_type`          ENUM ('RADIO', 'CHECKBOX', 'TEXT_FIELD', 'TEXT_AREA', 'DROPDOWN'),
    `correct_options_json` TEXT,
    `created_by_id`        BIGINT,
    `status`               ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`              TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`              TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`              TIMESTAMP                           NULL,
    `deleted_by_id`        BIGINT                              NULL,
    `version`              BIGINT                              NOT NULL
);


CREATE TABLE `answer_option`
(
    `id`            BIGINT PRIMARY KEY AUTO_INCREMENT,
    `label`         varchar(255),
    `correct`       BOOLEAN,
    `option_number` BIGINT,
    `question_id`   BIGINT,
    `created_by_id` BIGINT,
    `status`        ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`       TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`       TIMESTAMP                           NULL,
    `deleted_by_id` BIGINT                              NULL,
    `version`       BIGINT                              NOT NULL
);


CREATE TABLE `questionnaire`
(
    `id`                  BIGINT PRIMARY KEY AUTO_INCREMENT,
    `label`               varchar(255),
    `subject_id`          BIGINT,
    `chapters_json`       TEXT,
    `question_Order_json` TEXT,
    `created_by_id`       BIGINT,
    `status`              ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`             TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`             TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`             TIMESTAMP                           NULL,
    `deleted_by_id`       BIGINT                              NULL,
    `version`             BIGINT                              NOT NULL DEFAULT 0
);


CREATE TABLE `questionnaire_question`
(
    `id`               BIGINT PRIMARY KEY AUTO_INCREMENT,
    `question_number`  BIGINT,
    `questionnaire_id` BIGINT,
    `question_id`      BIGINT,
    `status`           ENUM ('DRAFT','APPROVED','DELETED') NOT NULL,
    `created`          TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`          TIMESTAMP                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted`          TIMESTAMP                           NULL,
    `deleted_by_id`    BIGINT                              NULL
);

ALTER TABLE `user`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `subject`
    ADD FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `subject`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `chapter`
    ADD FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`);

ALTER TABLE `chapter`
    ADD FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `chapter`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `question`
    ADD FOREIGN KEY (`chapter_id`) REFERENCES `chapter` (`id`);

ALTER TABLE `question`
    ADD FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `question`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `answer_option`
    ADD FOREIGN KEY (`question_id`) REFERENCES `question` (`id`);

ALTER TABLE `answer_option`
    ADD FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `answer_option`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `questionnaire`
    ADD FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`);

ALTER TABLE `questionnaire`
    ADD FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `questionnaire`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `questionnaire_question`
    ADD FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaire` (`id`);

ALTER TABLE `questionnaire_question`
    ADD FOREIGN KEY (`question_id`) REFERENCES `question` (`id`);

ALTER TABLE `questionnaire_question`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `questionnaire_assignment`
    ADD FOREIGN KEY (`deleted_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `questionnaire_assignment`
    ADD FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

ALTER TABLE `questionnaire_assignment`
    ADD FOREIGN KEY (`assignee_id`) REFERENCES `user` (`id`);

ALTER TABLE `questionnaire_assignment`
    ADD FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaire` (`id`);

CREATE UNIQUE INDEX `question_index_code` ON `question` (`code`);

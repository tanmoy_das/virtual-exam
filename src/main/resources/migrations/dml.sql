INSERT INTO `role` (`id`, `label`, `status`, `created`, `updated`, `deleted`)
VALUES (NULL, 'STUDENT', 'APPROVED', CURRENT_TIME(), CURRENT_TIME(), NULL),
       (NULL, 'TEACHER', 'APPROVED', CURRENT_TIME(), CURRENT_TIME(), NULL);
